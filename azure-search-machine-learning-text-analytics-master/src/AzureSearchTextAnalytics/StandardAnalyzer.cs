﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureSearchTextAnalytics
{
    [Newtonsoft.Json.JsonObject("#Microsoft.Azure.Search.StandardAnalyzer")]
    public class StandardAnalyzer : Microsoft.Azure.Search.Models.Analyzer
    {
       
        //public StandardAnalyzer(string name, Nullable<int> maxTokenLength = null, System.Collections.Generic.IList<string> stopwords = null)
        //{
        //    Name = name;
        //    Stopwords = Stopwords;
        //}

        [Newtonsoft.Json.JsonProperty(PropertyName = "maxTokenLength")]
        public Nullable<int> MaxTokenLength { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "stopwords")]
        public System.Collections.Generic.IList<string> Stopwords { get; set; }
       // public new string Name { get; set; }
    }
}
