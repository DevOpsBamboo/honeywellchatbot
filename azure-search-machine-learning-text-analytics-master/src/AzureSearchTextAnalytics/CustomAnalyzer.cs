﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.Search.Models;

namespace AzureSearchTextAnalytics
{
    [Newtonsoft.Json.JsonObject("#Microsoft.Azure.Search.CustomAnalyzer")]
    public class CustomAnalyzer : Microsoft.Azure.Search.Models.Analyzer

    {
       
        //public CustomAnalyzer(string name, string customTokenizerName, TokenFilterName[] tokenFilterName, CharFilterName[] charFilterName)
        //{
        //    Tokenizer = name;
        //    TokenFilters = tokenFilterName;
        //    CharFilters = charFilterName;
        //}

        [Newtonsoft.Json.JsonProperty(PropertyName = "charFilters")]
        public System.Collections.Generic.IList<Microsoft.Azure.Search.Models.CharFilterName> CharFilters { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "tokenFilters")]
        public System.Collections.Generic.IList<Microsoft.Azure.Search.Models.TokenFilterName> TokenFilters { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "tokenizer")]
        public Microsoft.Azure.Search.Models.TokenizerName Tokenizer { get; set; }

        [Newtonsoft.Json.JsonProperty(PropertyName = "stopwords")]
        public System.Collections.Generic.IList<string> Stopwords { get; set; }
    }

}
