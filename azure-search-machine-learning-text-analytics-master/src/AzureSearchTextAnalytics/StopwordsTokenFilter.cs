﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureSearchTextAnalytics
{
    [Newtonsoft.Json.JsonObject("#Microsoft.Azure.Search.StopwordsTokenFilter")]
    public class StopwordsTokenFilter : Microsoft.Azure.Search.Models.TokenFilter
    {
       
        [Newtonsoft.Json.JsonProperty(PropertyName = "ignoreCase")]
        public Nullable<bool> IgnoreCase { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "removeTrailing")]
        public Nullable<bool> RemoveTrailingStopWords { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "stopwords")]
        public System.Collections.Generic.IList<string> Stopwords { get; set; }
        [Newtonsoft.Json.JsonProperty(PropertyName = "stopwordsList")]
        public Nullable<Microsoft.Azure.Search.Models.StopwordsList> StopwordsList { get; set; }
    }
}
