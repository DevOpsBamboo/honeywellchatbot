﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureSearchTextAnalytics
{
    [Newtonsoft.Json.JsonObject("#Microsoft.Azure.Search.StopAnalyzer")]
    public class StopAnalyzer: Microsoft.Azure.Search.Models.Analyzer
    {
        [Newtonsoft.Json.JsonProperty(PropertyName = "stopwords")]
        public System.Collections.Generic.IList<string> Stopwords { get; set; }
    }
}
