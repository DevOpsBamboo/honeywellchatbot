﻿using System;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics;
using Microsoft.Azure.CognitiveServices.Language.TextAnalytics.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace AzureSearchTextAnalytics
{
    public class Program
    {
        private static string TextAnalyticsAPIKey = "a537b14fe5c945658ff03f73069b1b66";     // Learn more here: https://azure.microsoft.com/en-us/documentation/articles/machine-learning-apps-text-analytics/
       // static string searchServiceName = "arungskazsearch";     // Learn more here: https://azure.microsoft.com/en-us/documentation/articles/search-what-is-azure-search/
          static string searchServiceName = "gskazureserviceblobs";
        //static string searchServiceAPIKey = "2A609178B31368814F08DADDD0B58B65";
        static string searchServiceAPIKey = "0913192D3E48D67BF0BAB541210E54C4";
        

        //static string indexName = "textanalyticsindexquestion";
        static string indexName = "indexwithstopwords";
        static SearchServiceClient serviceClient = new SearchServiceClient(searchServiceName, new SearchCredentials(searchServiceAPIKey));
        static ISearchIndexClient indexClient = serviceClient.Indexes.GetClient(indexName);

        private const string ApiUri = "https://eastus.api.cognitive.microsoft.com/";
        private const string SubscriptionKey = "83d8d62a-89e5-4c46-93c9-2ae040b09849";
        // private const string Text = "The food was delicious and there were wonderful staff.";

        public static void Main(string[] args)
        {

            // Note, this will create a new Azure Search Index for the summary and the key phrases
            Console.WriteLine("\r\nCreating Azure Search index...");

            //LoadJSONFromFile();

              CreateIndex(serviceClient, indexName);

              UploaQandAdDocuments(indexClient);

        }


        private static HttpClient GetClient()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", SubscriptionKey);
            client.DefaultRequestHeaders.Add("ContentType", "application/json");
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            return client;
        }
        public static void LoadJSONFromFile()
        {
            //using (StreamReader sr = new StreamReader("C:\\Users\\kkanukun\\Desktop\\Chatbot\\latestjsonfiles\\home.json"))

            string cfgString = File.ReadAllText("C:\\Users\\kkanukun\\Desktop\\Chatbot\\latestjsonfiles\\home.json");


            

            string jsonObject = JObject.Parse(cfgString).ToString();

            JObject rss = JObject.Parse(jsonObject);

            JArray categories = (JArray)rss["componentSet"];

            //JArray jarr = (JArray)jsonObject["componentSet"];



            ////  JArray jsonObject = ((JArray)JsonConvert.DeserializeObject(cfgString));

            //var strContent = jsonObject[0]["componentName"].Value<string>();


            //foreach (var item in jarr)
            //{
            //    string strCname = item.SelectToken("content").ToString();
            //    if (strCname == "content")
            //    {
            //        if (item.SelectToken("content").ToString() != "" && item.SelectToken("content") != null)
            //        {
            //            string strQuestion = item.SelectToken("content").ToString();
            //        }
            //    }


            //}

        }

        public static List<Item> LoadJson()
        {
            List<Item> items = new List<Item>();
            using (StreamReader r = new StreamReader("C:\\Users\\kkanukun\\Desktop\\Chatbot\\Structured\\faq_file.json"))
            {
                string json = r.ReadToEnd();
                items = JsonConvert.DeserializeObject<List<Item>>(json);
            }
            return items;
        }

        public static List<Match> GetBestMatches(string[] sentences, List<string> words)
        {
            List<Match> matchList = new List<Match>();
            int counter = 0;
            foreach (var sentence in sentences)
            {
                double count = 0;

                Match match = new Match();
                foreach (var phrase in words)
                {
                    if ((sentence.ToLower().IndexOf(phrase.ToLower()) > -1) &&
                        (sentence.Length > 20) && (WordCount(sentence) >= 3))
                        count += 1;
                }

                if (count > 0)
                    matchList.Add(new Match { Sentence = counter, Total = count });
                counter++;
            }

            return matchList.OrderByDescending(x => x.Total).ToList();
        }


        public static int WordCount(string text)
        {
            // Calculate total word count in text
            int wordCount = 0, index = 0;

            while (index < text.Length)
            {
                // check if current char is part of a word
                while (index < text.Length && !char.IsWhiteSpace(text[index]))
                    index++;

                wordCount++;

                // skip whitespace until next word
                while (index < text.Length && char.IsWhiteSpace(text[index]))
                    index++;
            }

            return wordCount;
        }


        public static async Task<List<string>> ExtractKeyPhrases(string content)
        {
            var credentials = new ApiKeyServiceClientCredentials(TextAnalyticsAPIKey);
            List<string> strList = new List<string>();
            var client = new TextAnalyticsClient(credentials)
            {
                Endpoint = ApiUri
            };

            Console.OutputEncoding = System.Text.Encoding.UTF8;

            try
            {
                var inputDocuments = new MultiLanguageBatchInput(
                            new List<MultiLanguageInput>
                            {
                            new MultiLanguageInput("en", "1", content)
                            });

                var kpResults = await client.KeyPhrasesAsync(false, inputDocuments);

                foreach (var document in kpResults.Documents)
                {
                    Console.WriteLine($"Document ID: {document.Id} ");

                    Console.WriteLine("\t Key phrases:");

                    foreach (string keyphrase in document.KeyPhrases)
                    {
                        strList.Add(keyphrase);// Console.WriteLine($"\t\t{keyphrase}");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return strList;


        }


        public static void CreateIndex(SearchServiceClient serviceClient, string indexName)
        {

            System.String[] stopWords = new System.String[] { "body", "skin", "pain", "gel", "voltaren" };

            #region StopWords
            System.String[] strStopWords = new System.String[]
                {
                "ourselves", "hers", "between", "yourself", "but", "again", "there", "about", "once", "during", "out", "very",
                "having", "with", "they", "own", "an", "be", "some", "for", "do", "its", "yours", "such", "into", "of", "most",
                "itself", "other", "off", "is", "s", "am", "or", "who", "as", "from", "him", "each", "the", "themselves", "until",
                "below", "are", "we", "these", "your", "his", "through", "don", "nor", "me", "were", "her", "more", "himself", "this",
                "down", "should", "our", "their", "while", "above", "both", "up", "to", "ours", "had", "she", "all", "no", "when", "at",
                "any", "before", "them", "same", "and", "been", "have", "in", "will", "on", "does", "yourselves", "then", "that", "because", "what",
                "over", "why", "so", "can", "did", "not", "now", "under", "he", "you", "herself", "has", "just", "where", "too", "only", "myself",
                "which","those", "i", "after", "few", "whom", "t", "being", "if", "theirs", "my", "against",
                "a", "by", "doing", "it", "how", "further", "was", "here", "than"
                 };
            #endregion

            if (serviceClient.Indexes.Exists(indexName))
            {
                serviceClient.Indexes.Delete(indexName);
            }

            var definition = new Index()
            {
                Name = indexName,
                Fields = new[]
                {
                    new Field("id", DataType.String) { IsKey = true,IsRetrievable=true },
                    new Field("question", DataType.String) { IsSearchable = true, IsFilterable = false, IsSortable = false, IsFacetable = false },
                    new Field("answer", DataType.String) { IsSearchable = true, IsFilterable = false, IsSortable = false, IsFacetable = false },
                    new Field("url", DataType.String) { IsSearchable = false, IsFilterable = false, IsSortable = false, IsFacetable = false }
                   
                },
                Analyzers = new[]
                {
                    #region CustomAnalyzer commented Code 
                    //new CustomAnalyzer()
                    //{
                    //    Name = "Standard - Lucene",
                    //    Tokenizer = TokenizerName.UaxUrlEmail,
                    //    TokenFilters = new[] { TokenFilterName.Lowercase, TokenFilterName.Stopwords, TokenFilterName.Apostrophe }
                    //}
                    #endregion

                    new StopAnalyzer()
                    {
                        Name="Stop Analyzer",
                        Stopwords = stopWords.ToList()
                    },

                }

            };
            serviceClient.Indexes.Create(definition);

        }

        public static void UploadDocuments(ISearchIndexClient indexClient, string fileId, string summary, List<string> keyPhrases)
        {
            // This is really inefficient as I should be batching the uploads
            List<IndexAction> indexOperations = new List<IndexAction>();
            var doc = new Document();
            doc.Add("fileId", fileId);
            doc.Add("summary", summary);
            doc.Add("keyPhrases", keyPhrases);
            indexOperations.Add(IndexAction.Upload(doc));

            try
            {
                indexClient.Documents.Index(new IndexBatch(indexOperations));
            }
            catch (IndexBatchException e)
            {

                Console.WriteLine(
                "Failed to index some of the documents: {0}",
                       String.Join(", ", e.IndexingResults.Where(r => !r.Succeeded).Select(r => r.Key)));
            }

        }


        public static void UploaQandAdDocuments(ISearchIndexClient indexClient)
        {

            List<IndexAction> indexOperations = new List<IndexAction>();
            List<Item> items = LoadJson();

            if (items.Count > 0)
            {
                foreach (Item question in items)
                {
                    Item item = new Item();
                    var doc = new Document();
                    string content = string.Empty;
                    item.Id = question.Id;
                    item.topic = question.topic;
                    item.url = question.url;
                    item.question = question.question;
                    item.answer = question.answer;

                    content = item.answer;
                    //content = item.question;
                    // Take the top 20 key phrases
                    List<string> KeyPhrases = ExtractKeyPhrases(content).Result;

                    doc.Add("id", item.Id);
                    doc.Add("topic", item.topic);
                    doc.Add("url", item.url);
                    doc.Add("question", item.question);
                    doc.Add("answer", item.answer);
                    doc.Add("keyPhrases", KeyPhrases);
                    indexOperations.Add(IndexAction.Upload(doc));
                }

                try
                {

                    indexClient.Documents.Index(new IndexBatch(indexOperations));
                }
                catch (IndexBatchException e)
                {

                    Console.WriteLine(
                    "Failed to index some of the documents: {0}",
                           String.Join(", ", e.IndexingResults.Where(r => !r.Succeeded).Select(r => r.Key)));
                }

            }




        }

        public class Match
        {
            public int Sentence { get; set; }
            public double Total { get; set; }
        }
    }
}

