using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Sandboxable.Microsoft.WindowsAzure.Storage.Blob;
using Sandboxable.Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.KeyVault.Models;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Configuration;

namespace GSKAzureFunction
{
    public static class GSKAzureFunctionV1
    {
        [FunctionName("GSKAzureFunctionV1")]
        public static void Run([BlobTrigger("gskcontainer/{name}", Connection = "AzureWebJobsStorage")]Stream myBlob, string name, ILogger log, Microsoft.Azure.WebJobs.ExecutionContext context)
        {
            try
            {
                
                string keyAzureSearchServiceKey = Environment.GetEnvironmentVariable("AzureSearchServiceKey");
              //  Thread.Sleep(1000);
                log.LogInformation($"Started documents uploading to Index..");

                #region All Keys Commented 
                //"StorageConnectionString": "DefaultEndpointsProtocol=https;AccountName=gskstoageaccount;AccountKey=GC1fNqMn7J1guXyNaNFWaqXpda3bWKtrp9VVkPvpa4QwkmH1ME9pQ/m8AMEaw2kpew92JuYwsMTBeUrW0QOwMQ==;EndpointSuffix=core.windows.net",
                //"SearchServiceName": "gskazureserviceblobs",
                //"SearchServiceAPIKey": "0913192D3E48D67BF0BAB541210E54C4",
                //"IndexName": "gskindex",
                //"Container": "gskcontainer"

                //string searchServiceAPIKey = "C5644A8B52E097AA87363716D116A9A7";
                //string searchServiceName = "speechsearchrgdevsearch";
                //string indexName = "gskindex";
                //string Container = "gskcontainer";
                //string StorageConnectionString = "DefaultEndpointsProtocol=https;AccountName=speechsearchrgdevstore;AccountKey=uqC8R031mKspExA10up6Umir53zs8LpBeg5sFk7HzBrHYgjOGGdCinRDcJ6RH0oVhBiDdIHER9+BYoP007hRFA==;EndpointSuffix=core.windows.net";

                //string searchServiceAPIKey= config["SearchServiceAPIKey"];
                //string searchServiceName = config["SearchServiceName"];
                //string Container = config["Container"];
                //string StorageConnectionString = config["IndexName"];
                //string indexName = config["StorageConnectionString"];
                #endregion

                #region KeyVault Capgemini
                //var vaultAddress = "https://gskkeyvault.vault.azure.net/";
                string vaultAddress = Environment.GetEnvironmentVariable("KeyVaultURL");
                //KeyVaultURL
                KeyVaultClient keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(GetAccessToken));
                var secSearchServiceAPIKey = keyVaultClient.GetSecretAsync(vaultAddress, keyAzureSearchServiceKey, "c53f449714924498be87f514126b4d3a").GetAwaiter().GetResult();
                string searchServiceAPIKey = secSearchServiceAPIKey.Value;
                var secSearchServiceName = keyVaultClient.GetSecretAsync(vaultAddress, "SearchServiceName", "6315fa6edf6245798552de04b3ce1d41").GetAwaiter().GetResult();
                string searchServiceName = secSearchServiceName.Value;
                var secIndexName = keyVaultClient.GetSecretAsync(vaultAddress, "AzureSearchIndex", "b2fcabee9cdd4e78929ccf3d629206f0").GetAwaiter().GetResult();
                string indexName = secIndexName.Value;//config["Values:IndexName"];
                var secContainer = keyVaultClient.GetSecretAsync(vaultAddress, "BlobStorageContainer", "fdcc04a6ca0e4f6a97e7c704e18cca8a").GetAwaiter().GetResult();
                string Container = secContainer.Value;
                var secStorageConnString = keyVaultClient.GetSecretAsync(vaultAddress, "BlobStorageServiceKey", "5e3889ebb1ff483e8aac8717d38722b5").GetAwaiter().GetResult();
                string StorageConnectionString = secStorageConnString.Value;

                #endregion


                #region KeyVault GSK
                //var vaultAddress = "https://speechsearchdevkeyvault.vault.azure.net/";
                //KeyVaultClient keyVaultClient = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(GetAccessToken));
                //var secSearchServiceAPIKey = keyVaultClient.GetSecretAsync(vaultAddress, "AzuresSearchServiceAPIKey", "13b4f6d48a844cb38b2cfc5a9b7d9494").GetAwaiter().GetResult();
                //string searchServiceAPIKey = secSearchServiceAPIKey.Value;

                //var secSearchServiceName = keyVaultClient.GetSecretAsync(vaultAddress, "AzureSearchServiceName", "41518afca8b649f6b5594ec63e9f188a").GetAwaiter().GetResult();
                //string searchServiceName = secSearchServiceName.Value;

                //var secIndexName = keyVaultClient.GetSecretAsync(vaultAddress, "AzureSearchServiceIndexName", "a18d3ea3fb104b1f9aa0168b7bfa422e").GetAwaiter().GetResult();
                //string indexName = secIndexName.Value;

                //var secContainer = keyVaultClient.GetSecretAsync(vaultAddress, "BlobStorageContainerName", "1b3773425e8941d9b605889197fed01d").GetAwaiter().GetResult();
                //string Container = secContainer.Value;

                //var secStorageConnString = keyVaultClient.GetSecretAsync(vaultAddress, "BlobStorageConnectionString", "829696a20b4243efb2a32882fbc1a8ee").GetAwaiter().GetResult();
                //string StorageConnectionString = secStorageConnString.Value;

                #endregion

                SearchServiceClient serviceClient = new SearchServiceClient(searchServiceName, new SearchCredentials(searchServiceAPIKey));
                ISearchIndexClient indexClient = serviceClient.Indexes.GetClient(indexName);

                UploaQandAdDocuments(indexClient, StorageConnectionString, Container, indexName, serviceClient);

                log.LogInformation($"Ended documents uploading to Index..");
               
              //  Thread.Sleep(20000);
            }
            catch (Exception ex)
            {
                log.LogInformation($"Error Occured " + ex.StackTrace);
            }
        }

        #region GetAccessToken
        public static async Task<string> GetAccessToken(string authority, string resource, string scope)
        {
            //CapGemini Keys
            string clientId = Environment.GetEnvironmentVariable("ClientId");
            string clientSecret = Environment.GetEnvironmentVariable("ClientSecret");

            //var clientId = "22f1fef4-3fa5-42c0-a582-837fc1dd187f";
            //var clientSecret = "5fz2o?pY5-/d9lyQeTEEpUobK.xfN6Uj";

            //GSK Keys Dev Environment
            //var clientId = "da638192-a87d-4589-8ee3-8ae4a69bbde5";
            //var clientSecret = "D/mnSYv:mv4G=g0XN:0D8DB/297e*jIQ";

            ClientCredential clientCredential = new ClientCredential(clientId, clientSecret);
            var context = new AuthenticationContext(authority, TokenCache.DefaultShared);
            var result = await context.AcquireTokenAsync(resource, clientCredential);
            return result.AccessToken;
        }
        #endregion

        #region Creating Index
        private static void CreateIndex(string indexName, SearchServiceClient serviceClient)
        {
            //if (serviceClient.Indexes.Exists(indexName))
            //{
            //    serviceClient.Indexes.Delete(indexName);
            //}
            var definition = new Index()
            {
                Name = indexName,
                Fields = new[]
                {
                    new Field("id", DataType.String) { IsKey = true },
                    new Field("question", DataType.String) { IsSearchable = true, IsFilterable = false, IsSortable = false, IsFacetable = false },
                    new Field("answer", DataType.String) { IsSearchable = true, IsFilterable = false, IsSortable = false, IsFacetable = false },
                    new Field("url", DataType.String) { IsSearchable = false, IsFilterable = false, IsSortable = false, IsFacetable = false },
                    new Field("title", DataType.String) { IsSearchable = false, IsFilterable = false, IsSortable = false, IsFacetable = false },
                    new Field("description", DataType.String) { IsSearchable = false, IsFilterable = false, IsSortable = false, IsFacetable = false }

                }
            };
            Index index = serviceClient.Indexes.CreateOrUpdate(definition);
           // serviceClient.Indexes.Create(definition);
        }
        #endregion

        #region Uploading Documents
        private static void UploaQandAdDocuments(ISearchIndexClient indexClient, string strConn,
            string strContainer, string strIndexName, SearchServiceClient serviceClient)
        {
            Item item = new Item();
            item.ListEnumarble = GetBlob(strConn, strContainer);
            if (item.ListEnumarble != null && item.ListEnumarble.ToList().Count > 0)
            {
                CreateIndex(strIndexName, serviceClient);

                var itemsList = from p in item.ListEnumarble
                                select new
                                {
                                    p.id,
                                    p.answer,
                                    p.question,
                                    p.title,
                                    p.description,
                                    p.url

                                };


                var batch = IndexBatch.Upload(itemsList);

                try
                {
                    indexClient.Documents.Index(batch);

                }
                catch (IndexBatchException e)
                {
                    Console.WriteLine(
                      "Failed to index some of the documents: {0}",
                      String.Join(", ", e.IndexingResults.Where(r => !r.Succeeded).Select(r => r.Key)));
                }

                Thread.Sleep(2000);

            }

        }
        #endregion

        #region Getting Blob Details
        public static IEnumerable<Item> GetBlob(string strConn, string strContainer)
        {

            //string StorageConnectionString = "DefaultEndpointsProtocol=https;AccountName=gskstoageaccount;AccountKey=GC1fNqMn7J1guXyNaNFWaqXpda3bWKtrp9VVkPvpa4QwkmH1ME9pQ/m8AMEaw2kpew92JuYwsMTBeUrW0QOwMQ==;EndpointSuffix=core.windows.net";
            string StorageConnectionString = strConn;
            var storageAccount = CloudStorageAccount.Parse(StorageConnectionString);
            string Container = strContainer;
            // Connect to the blob storage
            CloudBlobClient serviceClient = storageAccount.CreateCloudBlobClient();
            // Connect to the blob container
            var container = serviceClient.GetContainerReference(Container);
            var list = container.ListBlobs();
            List<string> blobNames = list.OfType<CloudBlockBlob>().Select(b => b.Name).ToList();
            string contents = string.Empty;
            List<Item> itemsParents = new List<Item>();
            int intid = 99;
            foreach (string strFiname in blobNames)
            {
                if (strFiname.Contains(".json"))
                {
                    CloudBlockBlob blob = container.GetBlockBlobReference(strFiname);
                    // Get the blob file as text
                    contents = blob.DownloadTextAsync().Result;

                    List<Item> items = new List<Item>();
                    string json = contents;
                    items = JsonConvert.DeserializeObject<List<Item>>(json);
                    if (items.Count > 0)
                        foreach (Item item in items)
                        {

                            item.id = "0";
                            item.id = (intid + 1).ToString();
                            intid = Convert.ToInt32(item.id);
                            itemsParents.Add(item);
                        }
                   // itemsParents.AddRange(items);
                }


            }

            return itemsParents;
        }
        #endregion
    }
}
