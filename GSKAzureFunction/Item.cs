﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GSKAzureFunction
{
    public class Item
    {
        public string id;
        public string title;
        public string url;
        public string question;
        public string answer;
        public string description;
        public IEnumerable<Item> ListEnumarble;
    }
}
