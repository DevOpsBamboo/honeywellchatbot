﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System.Configuration;
using Newtonsoft.Json;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace GSKAzureSearch
{
    public partial class _Default : Page
    {
        private  SearchServiceClient _searchClient;
        private  ISearchIndexClient _indexClient;
        private  string IndexName = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                string searchServiceName = ConfigurationManager.AppSettings["SearchServiceName"];
                string apiKey = ConfigurationManager.AppSettings["SearchServiceApiKey"];

                // Create a reference to the NYCJobs index
                _searchClient = new SearchServiceClient(searchServiceName, new SearchCredentials(apiKey));
               
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            List<Answers> listAnswers = new List<Answers>();

            IndexName = "indexwithstopwords";
            _indexClient = _searchClient.Indexes.GetClient(IndexName);
            SearchParameters sp1 = new SearchParameters()
            {
                Select = new[] { "answer", "keyPhrases", "question", "id" }

            };
            string strSearhKey = txtSearchKey.Text;
            strSearhKey = strSearhKey.Replace(' ', '%');


            DocumentSearchResult<Document> searchResults = Search(_indexClient, strSearhKey);

            foreach (var result1 in searchResults.Results)
            {
                Answers ans = new Answers();
                ans.id = Convert.ToInt32(result1.Document["id"]);
                ans.Answer = result1.Document["answer"].ToString();
                listAnswers.Add(ans);

            }

            grid1.DataSource = listAnswers;
            grid1.DataBind();
        }

      

        protected void btnSearch_1_Click(object sender, EventArgs e)
        {
            List<Answers> listAnswers = new List<Answers>();
            IndexName = "textanalyticsindexquestion";
            _indexClient = _searchClient.Indexes.GetClient(IndexName);
            string strSearhKey = txtSearchKey1.Text;
            strSearhKey = strSearhKey.Replace(' ', '%');

            DocumentSearchResult<Document> searchResults = Search(_indexClient, strSearhKey);

            foreach (var result1 in searchResults.Results)
            {
                Answers ans = new Answers();
                ans.id = Convert.ToInt32(result1.Document["id"]);
                ans.Answer = result1.Document["answer"].ToString();
                listAnswers.Add(ans);

            }

            grid2.DataSource = listAnswers;
            grid2.DataBind();

        }

        protected void btnSearch_2_Click(object sender, EventArgs e)
        {
            //cosmosdb - index2
            List<Answers> listAnswers = new List<Answers>();
            IndexName = "cosmosdb-index2";
            _indexClient = _searchClient.Indexes.GetClient(IndexName);
             string strSearhKey = txtSearchKey2.Text;
            strSearhKey = strSearhKey.Replace(' ', '%');

            DocumentSearchResult<Document> searchResults = Search(_indexClient, strSearhKey);

            foreach (var result1 in searchResults.Results)
            {
                Answers ans = new Answers();
                ans.id = Convert.ToInt32(result1.Document["id"]);
                ans.Answer = result1.Document["answer"].ToString();
                listAnswers.Add(ans);

            }

            grid2.DataSource = listAnswers;
            grid2.DataBind();

        }

        private static DocumentSearchResult<Document> Search(ISearchIndexClient indexClient, string strTerm)
        {
            var parameters =
                new SearchParameters
                {
                    Select = new[] { "answer", "keyPhrases", "question", "topic","id" }
                };

            return indexClient.Documents.Search<Document>(strTerm, parameters);
        }
    }
}