﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GSKAzureSearch._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h4>Azure Search with Key Phrases on Answer</h4>
        <asp:TextBox ID="txtSearchKey" runat="server" Width="425px"></asp:TextBox>&nbsp;<asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-primary" OnClick="btnSearch_Click" />
       <asp:GridView ID="grid1" runat="server"></asp:GridView>
        <br />
        <h4>Azure Search with Key Phrases on Question</h4>
        <asp:TextBox ID="txtSearchKey1" runat="server" Width="425px"></asp:TextBox>&nbsp;<asp:Button ID="btnSearch_1" runat="server" Text="Search" class="btn btn-primary" OnClick="btnSearch_1_Click" />
         <asp:GridView ID="grid2" runat="server"></asp:GridView>
        <br />
        <h4>Azure Search with Key Phrases on Unstructured Data</h4>
        <asp:TextBox ID="txtSearchKey2" runat="server" Width="425px"></asp:TextBox>&nbsp;<asp:Button ID="btnSearch_2" runat="server" Text="Search" class="btn btn-primary" OnClick="btnSearch_2_Click" />

        <%--<p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>--%>
    </div>

    
</asp:Content>
