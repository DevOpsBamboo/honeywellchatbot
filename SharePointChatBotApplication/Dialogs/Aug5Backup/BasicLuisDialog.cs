﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Search.Query;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;
using OfficeDevPnP.Core;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
//using SharePointPnPPowerShellOnline
//using Microsoft.CognitiveServices.Speech;


namespace Microsoft.Bot.Sample.LuisBot
{
    // For more information about this template visit http://aka.ms/azurebots-csharp-luis
    [Serializable]
    public class BasicLuisDialog : LuisDialog<object>
    {
        public BasicLuisDialog() : base(new LuisService(new LuisModelAttribute(
            ConfigurationManager.AppSettings["LuisAppId"],
            ConfigurationManager.AppSettings["LuisAPIKey"],
            domain: ConfigurationManager.AppSettings["LuisAPIHostName"])))
        {
        }

        private const string fiverate = "★★★★★";
        private const string fourrate = "★★★★";
        private const string threerate = "★★★";
        private const string tworate = "★★";
        private const string Onerate = "★";

        [LuisIntent("None"), LuisIntent("none"), LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult luisResult)
        {
            string[] s1 = new string[] { "Say that one more time?", "Sorry, could you say that again?", "Can you say that again?",
                "I didn't get that. Can you say it again?", "I missed what you said.What was that ? ", "Sorry, can you say that again?", "Sorry, what was that?", "One more time?", "What was that ?",
                "I didn't get that. Can you repeat?","I missed that, say that again?"};
            Random rnd = new Random();
            int randIndex = rnd.Next(10);
            var randomString = s1[randIndex];
            await context.PostAsync(String.Format(randomString));
            context.Wait(this.MessageReceived);
        }

        [LuisIntent("Default ")]
        public async Task Default(IDialogContext context, LuisResult luisResult)
        {
            await context.PostAsync("Sorry.. I am still learning. I can't answer for this now.");
            context.Wait(this.MessageReceived);
        }

        [LuisIntent("SearchDocument")]
        public async Task FindDocument(IDialogContext context, LuisResult luisResult)
        {
           // var urlref = window.location.href;
            var token = await context.GetUserTokenAsync(ConfigurationManager.AppSettings["ConnectionName"]).ConfigureAwait(false);
            if (token != null)
            {
                try
                {
                    //EntityRecommendation documentName;
                    SharePointLogin sharePointLogin = new SharePointLogin();
                    //sharePointLogin.SiteUrl = ConfigurationManager.AppSettings["siteUrl"];
                    //sharePointLogin.EmailAddress = ConfigurationManager.AppSettings["emailAddress"];
                    //sharePointLogin.Password = ConfigurationManager.AppSettings["password"];
                    await DocumentSearch(context, luisResult, sharePointLogin);
                    //need to pass luisResult and then change the code accordingly..
                    //await SiteSearch(context, luisResult, sharePointLogin);
                }
                catch (Exception e)
                {
                    var exc = e.Message;
                }
            }
            else
            {
                // If Bot Service does not have a token, send an OAuth card to sign in
                await SendOAuthCardAsync(context, (Activity)context.Activity);
            }
        }
        //***** Meeting Planner code start *****
        [LuisIntent("message")]
        public async Task message(IDialogContext context, LuisResult luisResult)
        {
            // var urlref = window.location.href;
            var token = await context.GetUserTokenAsync(ConfigurationManager.AppSettings["ConnectionName"]).ConfigureAwait(false);
            if (token != null)
            {
                try
                {
                    var token1 = "eyJ0eXAiOiJKV1QiLCJub25jZSI6IkFRQUJBQUFBQUFBUDB3TGxxZExWVG9PcEE0a3d6U254MUFISU9mUi00Ql9ZaC1EOXE3Wk8zNHROWklVRjM4eGRHdnI3RmptWFJZdGliM1RrUk9sVmQtVDZtRTlyZHRZeUZWS09IR0xEenNSQnd0UndIckgySWlBQSIsImFsZyI6IlJTMjU2IiwieDV0IjoidTRPZk5GUEh3RUJvc0hqdHJhdU9iVjg0TG5ZIiwia2lkIjoidTRPZk5GUEh3RUJvc0hqdHJhdU9iVjg0TG5ZIn0.eyJhdWQiOiJodHRwczovL2dyYXBoLm1pY3Jvc29mdC5jb20iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC83NmEyYWU1YS05ZjAwLTRmNmItOTVlZC01ZDMzZDc3YzRkNjEvIiwiaWF0IjoxNTYyODI3OTg1LCJuYmYiOjE1NjI4Mjc5ODUsImV4cCI6MTU2MjgzMTg4NSwiYWNjdCI6MCwiYWNyIjoiMSIsImFpbyI6IkFTUUEyLzhNQUFBQTRPbkd4Mzk2aWVLQWROc2xCNGVUNGZvNU9PbEFVTGd0N2NIVWJER0tVTlE9IiwiYW1yIjpbIndpYSJdLCJhcHBfZGlzcGxheW5hbWUiOiJNeSBQeXRob24gQXBwIiwiYXBwaWQiOiIyNTQyZWEwNi00OTMyLTQwN2QtYTU4My05ZGE4ZGViNDQ0YjMiLCJhcHBpZGFjciI6IjEiLCJmYW1pbHlfbmFtZSI6Ik1laHJvdHJhIiwiZ2l2ZW5fbmFtZSI6IkFheXVzaCIsImluX2NvcnAiOiJ0cnVlIiwiaXBhZGRyIjoiMTQuMTQzLjE4Ni41MiIsIm5hbWUiOiJNZWhyb3RyYSwgQWF5dXNoIiwib2lkIjoiNTQ4OTRhZTEtY2MxNC00ZDQzLWFmMTMtNDVjZjk1MWM4Mzk0Iiwib25wcmVtX3NpZCI6IlMtMS01LTIxLTE1MzEwODIzNTUtNzM0NjQ5NjIxLTM3ODI1NzQ4OTgtMzM5NTQxMCIsInBsYXRmIjoiMyIsInB1aWQiOiIxMDAzMDAwMEFGNDZBMDBEIiwic2NwIjoiQ2FsZW5kYXJzLlJlYWRXcml0ZSBVc2VyLlJlYWQgVXNlci5SZWFkV3JpdGUgcHJvZmlsZSBvcGVuaWQgZW1haWwiLCJzdWIiOiJKM3M4TEgxajI1a1BEMG5hejVyalNQYV9hQVpDbHZUcFNUUFhaWDRVUWcwIiwidGlkIjoiNzZhMmFlNWEtOWYwMC00ZjZiLTk1ZWQtNWQzM2Q3N2M0ZDYxIiwidW5pcXVlX25hbWUiOiJhYXl1c2gubWVocm90cmFAY2FwZ2VtaW5pLmNvbSIsInVwbiI6ImFheXVzaC5tZWhyb3RyYUBjYXBnZW1pbmkuY29tIiwidXRpIjoiTW5kNHhKakItRU9kUE5WMVRmMUNBQSIsInZlciI6IjEuMCIsInhtc19zdCI6eyJzdWIiOiJnZmdQRGZzOGJ1aVJaZjVpT1BtTUtyYl9vdExKWXhHeGFmd3FoazRJM2Y4In0sInhtc190Y2R0IjoxMjg5MzE0OTYwfQ.bhvTyxCvq-npXw6FtqEG-9tA1L_Si7rR31XxH_XUjmojnoB6p3ikzHWGjnC5Bj2efS76vAOriF5shEooTJNDy5DSb3gtfgrdr4j_HL8hTnSMtH7PMNa3e3vHfEz6SW-zBRKdtcSdpFeSTMt7dL2EX_zKuc0sa-NK29O3HSsWAstFfaM4pwdP9FiYETeMsxv9Q6faKsJ86_DbQDrpmYno4-b4FDJHQLEUckuFx37TQHr51QnQfqUfPQip5qadsTbh-047ch93Fo6b3y7gxViBDHIM1COdIfYcjuHy4suXVFhpEW-ocv0HnMxXqHgGe339-7paHhvAFzqIFC2mpD-54Q";
                    string restUrl = String.Format("https://meetingplanner.azurewebsites.net/get?token={0}&msg={1}", token1, luisResult.Query.ToString());
                    HttpWebRequest request = WebRequest.Create(restUrl) as HttpWebRequest;
                    request.Method = "Get";
                    request.Accept = "application/json";
                    request.ContentType = "application/json";
                    string response = GetResponseFromJson(request.GetResponse() as HttpWebResponse);
                    //await context.PostAsync(response);
                    await context.SayAsync(text: response, speak: response);
                }
                catch (Exception e)
                {
                    var exc = e.Message;
                }
            }
            else
            {
                // If Bot Service does not have a token, send an OAuth card to sign in
                await SendOAuthCardAsync(context, (Activity)context.Activity);
            }
        }
        public string GetResponseFromJson(HttpWebResponse response)
        {
            string response1 = "";
            if (response.StatusCode == HttpStatusCode.OK)
            {
                JavaScriptSerializer json = new JavaScriptSerializer();
                StreamReader sr = new StreamReader(response.GetResponseStream());
                response1 = sr.ReadToEnd();               
            }
            return response1;
        }
        //***** Meeting Planner code End *****

        private async Task SendOAuthCardAsync(IDialogContext context, Activity activity)
        {
            await context.PostAsync($"To do this, you'll first need to sign in.");
            var reply = await context.Activity.CreateOAuthReplyAsync(ConfigurationManager.AppSettings["ConnectionName"], "Please Login to Authenticate", "LogIn", true).ConfigureAwait(false);
            await context.PostAsync(reply);
            context.Wait(WaitForToken);
        }

        private async Task WaitForToken(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;

            var tokenResponse = activity.ReadTokenResponseContent();
            if (tokenResponse != null)
            {
                // Use the token to do exciting things!
            }
            else
            {
                if (!string.IsNullOrEmpty(activity.Text))
                {
                    tokenResponse = await context.GetUserTokenAsync(ConfigurationManager.AppSettings["ConnectionName"],
                                                                       activity.Text);
                    if (tokenResponse != null)
                    {
                        // Use the token to do exciting things!                                               
                        context.EndConversation(tokenResponse.Token.ToString());
                        await context.PostAsync($"Logged in Successfully.. Please continue...");
                        return;
                    }
                }
                await context.PostAsync($"Hmm. Something went wrong. Let's try again.");
                await SendOAuthCardAsync(context, activity);
            }
        }

        [LuisIntent("signout")]
        public async Task Signout(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                bool signout = await context.SignOutUserAsync(ConfigurationManager.AppSettings["ConnectionName"]);
                await context.PostAsync($"Logged out Successfully.. Thank you.");
            }
            catch (Exception e)
            {
                var exc = e.Message;
            }

        }

        public async Task FeedBack1(IDialogContext context)
        {
            var feedback = ((Activity)context.Activity).CreateReply("How do you rate your conversation with me?");
            feedback.SuggestedActions = new SuggestedActions()
            {
                Actions = new List<CardAction>()
                {
                    new CardAction(){ Title = fiverate, Type=ActionTypes.PostBack, Value=$"fiverate" },
                    new CardAction(){ Title = fourrate, Type=ActionTypes.PostBack, Value=$"fourrate" },
                    new CardAction(){ Title = threerate, Type=ActionTypes.PostBack, Value=$"threerate" },
                    new CardAction(){ Title = tworate, Type=ActionTypes.PostBack, Value=$"tworate" },
                    new CardAction(){ Title = Onerate, Type=ActionTypes.PostBack, Value=$"Onerate" }

                }
            };

            await context.PostAsync(feedback);

            context.Wait(this.Message1ReceivedAsync);
        }
        public async Task Message1ReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var userFeedback = await result;
            //await context.PostAsync($"fiverate");
            if (userFeedback.Text.Contains("fiverate") || userFeedback.Text.Contains("fourrate") || userFeedback.Text.Contains("threerate") || userFeedback.Text.Contains("tworate") || userFeedback.Text.Contains("onerate"))
            {
                if (userFeedback.Text.Contains("fiverate") || userFeedback.Text.Contains("fourrate") || userFeedback.Text.Contains("threerate"))
                {
                    await context.PostAsync("Thanks for your valuable feedback!");
                }
                else if (userFeedback.Text.Contains("tworate") || userFeedback.Text.Contains("onerate"))
                {
                    await context.PostAsync("Thanks for your feedback!. We will try to improve our service..");
                }
                context.Done<IMessageActivity>(null);
            }
            else
            {
                await context.PostAsync("Thank you...");
                context.Done<IMessageActivity>(userFeedback);
            }
        }

        [LuisIntent("Welcome")]
        public async Task WelcomeMessage(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                //await context.PostAsync(String.Format("Hi, I am CMS Search Bot. I can search for documents from CMS Portal based on metatags available, for instance you can try... \n I am looking for <document type> documents \n Open <Industry> documents \n Can you show me <document type> within <Industry> industry \n\n You can even check for available meta tags, examples of industry, document types etc… as follows \n What are examples of meta tags? \n What are examples of Industry types?"));
                await context.SayAsync(text: String.Format("Hi, I am CMS Search Bot. I can search for documents from CMS Portal based on metatags available, for instance you can try... \n I am looking for <document type> documents \n Open <Industry> documents \n Can you show me <document type> within <Industry> industry \n\n You can even check for available meta tags, examples of industry, document types etc… as follows \n What are examples of meta tags? \n What are examples of Industry types?"), speak: "Hello Sreemukha");
            }
            catch (Exception e)
            {
                var exc = e.Message;
            }
            //await FeedBack1(context);  // Working Feedback
        }        

        [LuisIntent("NavigateSite")]
        public async Task SearchPage(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                //EntityRecommendation documentName;
                SharePointLogin sharePointLogin = new SharePointLogin();
                sharePointLogin.SiteUrl = ConfigurationManager.AppSettings["siteUrl"];
                sharePointLogin.EmailAddress = ConfigurationManager.AppSettings["emailAddress"];
                sharePointLogin.Password = ConfigurationManager.AppSettings["password"];

                //need to pass luisResult and then change the code accordingly..
                await SiteSearch(context, luisResult, sharePointLogin);
            }
            catch (Exception e)
            {
                var exc = e.Message;
            }

        }

        [LuisIntent("HRPolicy")]
        public async Task FindQnAIntent(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                //Ask the CRM HR knowledge base
                var qnaMakerAnswer = await GetAnswer(luisResult.Query);
                await context.PostAsync($"{qnaMakerAnswer}");
                context.Wait(MessageReceived);

            }
            catch (Exception e)
            {
                var exc = e.Message;
            }
        }

        [LuisIntent("GeneralTalk")]
        public async Task FindQnAIntentForGeneralTalk(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                var qnaMakerAnswer = await GetAnswer(luisResult.Query);
                await context.PostAsync($"{qnaMakerAnswer}");
                context.Wait(MessageReceived);
            }
            catch (Exception e)
            {
                var exc = e.Message;
            }
        }

        [LuisIntent("DocumentSearchRFI")]
        public async Task FindDocumentRFI(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                SharePointLogin sharePointLogin = new SharePointLogin();
                sharePointLogin.SiteUrl = ConfigurationManager.AppSettings["siteUrl"];
                sharePointLogin.EmailAddress = ConfigurationManager.AppSettings["emailAddress"];
                sharePointLogin.Password = ConfigurationManager.AppSettings["password"];
                if (luisResult.Entities.Count > 0)
                {
                    for (int i = 0; i < luisResult.Entities.Count; i++)
                    {
                        var entity = luisResult.Entities[i].Entity;
                        if (entity == "rfi" || entity == "rfp" || entity == "rfi / rfp" || entity == "rfp / rfi" || entity == "request for information")
                        {
                            luisResult.Entities[i].Entity = "rfi/rfp";
                        }
                    }
                }
                await DocumentSearch(context, luisResult, sharePointLogin);



                //string siteUrl = "https://capgemini.sharepoint.com/sites/KIM/Marketing/Pages/default.aspx";
                //string clientId = "<client-id>";
                //string clientSecret = "<client-secret>";

                //using (var clientContext = new AuthenticationManager().GetAppOnlyAuthenticatedContext(siteUrl, clientId, clientSecret))
                //{
                //    Web oWebsite = clientContext.Web;
                //    ListCollection collList = oWebsite.Lists;
                //    clientContext.Load(collList);
                //    clientContext.ExecuteQuery();
                //}
                //await DocumentSearch(context, luisResult, clientcontext);
            }
            catch (Exception e)
            {
                var exc = e.Message;
            }
        }
        public static async Task<string> GetAccessToken(string authority, string resource, string scope)
        {
            var clientId = "f3ea8e87-1576-4380-adf5-bab51f67a8d5";
            var clientSecret = "TVM6ZC8rGNi2rz/xFQVvF14upyBoYNds0bzAne1Ryq0=";
            ClientCredential clientCredential = new ClientCredential(clientId, clientSecret);
            var context = new AuthenticationContext(authority, TokenCache.DefaultShared);
            var result = await context.AcquireTokenAsync(resource, clientCredential);
            return result.AccessToken;
        }
        private async Task DocumentSearch(IDialogContext context, LuisResult luisResult, SharePointLogin spLogin)
        {
            Microsoft.Bot.Connector.IMessageActivity responseMessage = Activity.CreateMessageActivity();

            //ClientContext clientContext = new ClientContext(spLogin.SiteUrl);
            //clientContext.Credentials = new NetworkCredential(spLogin.EmailAddress, spLogin.Password);
            //clientContext.AuthenticationMode = ClientAuthenticationMode.Default;

            string siteUrl = "https://capgemini.sharepoint.com/sites/KIM/Practices/";
            string appId = "bc06b35f-59ae-40a4-9928-144cf0a0c312";
            string secretKey = "bTgKOUSD3/oWB27g4OGgAvwwJHkvQI+iM2EY6eJWK/w=";
            OfficeDevPnP.Core.AuthenticationManager authManager = new OfficeDevPnP.Core.AuthenticationManager();
            //TokenHelper.GetClientContextWithAccessToken(siteUrl, accessToken)
            //string accessToken = TokenHelper.GetAppOnlyAccessToken(TokenHelper.SharePointPrincipal, new Uri(siteUrl).Authority, realm).AccessToken;
            ClientContext clientContext = authManager.GetAppOnlyAuthenticatedContext(siteUrl, appId, secretKey);

            //clientContext.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
            //clientContext.FormsAuthenticationLoginInfo = new FormsAuthenticationLoginInfo(spLogin.EmailAddress, spLogin.Password);
            clientContext.ExecuteQuery();
            var x = luisResult;
            if (luisResult.Entities.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(string.Format("{0}:\"{1}\"", luisResult.Entities[0].Type, luisResult.Entities[0].Entity));

                for (int i = 1; i < luisResult.Entities.Count; i++)
                {
                    string validOperator = (luisResult.Entities[i - 1].Type == luisResult.Entities[i].Type) ? " OR " : " AND ";
                    sb.Append(validOperator);
                    //sb.Append(" NOT ");
                    sb.Append(string.Format("{0}:\"{1}\"", luisResult.Entities[i].Type, luisResult.Entities[i].Entity));
                }

                string keyvalue = sb.ToString();
                KeywordQuery keywordQuery = new KeywordQuery(clientContext);
                //keywordQuery.QueryText = string.Concat(keyvalue.ToString(), " IsDocument:1");
                keywordQuery.QueryText = string.Concat(keyvalue.ToString());

                keywordQuery.RowLimit = 10;
                SearchExecutor searchExecutor = new SearchExecutor(clientContext);
                ClientResult<ResultTableCollection> results = searchExecutor.ExecuteQuery(keywordQuery);
                clientContext.ExecuteQuery();

                string entities = this.BotEntityRecognition(luisResult);
                string roundedScore = luisResult.Intents[0].Score != null ? (Math.Round(luisResult.Intents[0].Score.Value, 2).ToString()) : "0";

                context.Wait(MessageReceived);

                if (results.Value != null && results.Value.Count > 0 && results.Value[0].RowCount > 0)
                {
                    responseMessage.Text += "I found some potential interesting reading for you!";
                    BuildReply(results, responseMessage);
                }
                else
                {
                    Boolean QueryTransformed = true;
                    if (QueryTransformed)
                    {
                        keywordQuery.QueryText = string.Concat(keyvalue.Replace("?", ""), " IsDocument:1");
                        keywordQuery.RowLimit = 3;
                        searchExecutor = new SearchExecutor(clientContext);
                        results = searchExecutor.ExecuteQuery(keywordQuery);
                        clientContext.ExecuteQuery();
                        if (results.Value != null && results.Value.Count > 0 && results.Value[0].RowCount > 0)
                        {
                            responseMessage.Text += "I found some potential interesting reading for you!";
                            BuildReply(results, responseMessage);
                        }
                        else
                            responseMessage.Text += "I could not find any interesting document!";
                    }
                    else
                        responseMessage.Text += "I could not find any interesting document!";
                }
            }
            Double roundedScoreNew = luisResult.Intents[0].Score != null ? Math.Round(luisResult.Intents[0].Score.Value, 2) : 0.0;
            if (responseMessage.Attachments.Count < 1 && roundedScoreNew < 0.81)
            {
                await context.PostAsync(String.Format("Sorry I did not understand you."));
            }
            else
            {
                await context.PostAsync(String.Format("I found {0} documents matching your criteria.", responseMessage.Attachments.Count));
                await this.DisplayThumbnailCard(context, responseMessage);
            }
        }


        private async Task SiteSearch(IDialogContext context, LuisResult luisResult, SharePointLogin spLogin)
        {
            Microsoft.Bot.Connector.IMessageActivity responseMessage = Activity.CreateMessageActivity();
            string sharepointHostWebUrl = spLogin.SiteUrl;
            if (luisResult.Entities.Count > 0)
            {
                string keyvalue = luisResult.Entities[0].Entity;
                string searchRestUrl = string.Format("/_api/search/query?querytext='{0}'&querytemplate='IsDocument:false'", keyvalue);
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(sharepointHostWebUrl.ToString() + searchRestUrl);
                request.Credentials = new NetworkCredential(spLogin.EmailAddress, spLogin.Password);
                request.Method = "GET";
                request.Accept = "application/xml";
                //request.Headers.Add("Content-Type", "application/xml");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                // process response..
                XDocument oDataXML = XDocument.Load(response.GetResponseStream(), LoadOptions.PreserveWhitespace);
                XNamespace d = "http://schemas.microsoft.com/ado/2007/08/dataservices";
                XNamespace m = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";

                List<XElement> items = oDataXML.Descendants(d + "query")
                                         .Elements(d + "PrimaryQueryResult")
                                         .Elements(d + "RelevantResults")
                                         .Elements(d + "Table")
                                         .Elements(d + "Rows")
                                         .Elements(d + "element")
                                         .ToList();

                // N.B. there might be a more elegant/efficient way of extracting the values from the (slightly awkward) XML than this.. 
                var searchResults = (from item in items
                                     select new ResultantTable
                                     {
                                         Title = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "Title").Parent.Element(d + "Value").Value,
                                         Description = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "Description").Parent.Element(d + "Value").Value,
                                         FileExtension = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "FileExtension").Parent.Element(d + "Value").Value,
                                         HitHighlightedSummary = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "HitHighlightedSummary").Parent.Element(d + "Value").Value,
                                         Path = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "Path").Parent.Element(d + "Value").Value,
                                         ServerRedirectedURL = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "ServerRedirectedURL").Parent.Element(d + "Value").Value
                                     }).ToList();

                searchResults = searchResults.Where(r => r.Title.Trim().ToLower().Contains(keyvalue.Trim().ToLower())).ToList();

                if (searchResults != null && searchResults.Count > 0)
                {
                    responseMessage.Text += "I found some potential interesting reading for you!";
                    BuildReplyForSite(searchResults, responseMessage);
                }
                await context.PostAsync(String.Format("I found {0} documents matching your criteria.", responseMessage.Attachments.Count));
                await this.DisplayThumbnailCard(context, responseMessage);
            }
        }

        static void BuildReply(ClientResult<ResultTableCollection> results, Microsoft.Bot.Connector.IMessageActivity reply)
        {
            foreach (var row in results.Value[0].ResultRows)
            {
                Microsoft.Bot.Connector.Attachment attachment = GetProfileThumbnailCard(row);

                reply.Attachments.Add(attachment);
            }
        }
        /// <summary>
        /// GetProfileThumbnailCard
        /// </summary>
        /// <returns></returns>
        private static Microsoft.Bot.Connector.Attachment GetProfileThumbnailCard(IDictionary<string, object> row)
        {
            List<CardAction> cardButtons = new List<CardAction>();
            List<CardImage> cardImages = new List<CardImage>();
            string ct = string.Empty;
            string icon = string.Empty;
            switch (row["FileExtension"].ToString())
            {
                case "docx":
                    ct = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    icon = "https://cdn3.iconfinder.com/data/icons/logos-brands-3/24/logo_brand_brands_logos_word-32.png";
                    break;
                case "xlsx":
                    ct = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    icon = "https://cdn3.iconfinder.com/data/icons/logos-brands-3/24/logo_brand_brands_logos_excel-32.png";
                    break;
                case "pptx":
                    ct = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                    icon = "https://cdn3.iconfinder.com/data/icons/logos-brands-3/24/logo_brand_brands_logos_powerpoint-32.png";
                    break;
                case "pdf":
                    ct = "application/pdf";
                    icon = "https://cdn4.iconfinder.com/data/icons/CS5/32/ACP_PDF%202_file_document.png";
                    break;

            }
            cardButtons.Add(new CardAction
            {
                Title = "Open",
                Value = (row["ServerRedirectedURL"] != null) ? row["ServerRedirectedURL"].ToString() : row["Path"].ToString(),
                Type = ActionTypes.OpenUrl
            });
            cardImages.Add(new CardImage(url: icon));
            var author = (row["Author"] != null) ? "Author: " + row["Author"].ToString() : "";
            String lastUpdatedTime = (row["LastModifiedTime"] != null) ? row["LastModifiedTime"].ToString() : "";
            string lastUpdateDate = "";
            if (lastUpdatedTime != "")
            {
                string[] lastUpdate = lastUpdatedTime.Split();
                lastUpdateDate = lastUpdate[0];
            }
            var thumbnailCard = new ThumbnailCard
            {
                // title of the card
                Title = (row["Title"] != null) ? row["Title"].ToString() : "Untitled",
                Subtitle = author + " / " + "Last Updated Date: " + lastUpdateDate,
                //Detail Text
                Text = (row["Description"] != null) ? row["Description"].ToString() : string.Empty,
                // smallThumbnailCard  Image
                Images = cardImages,
                // list of buttons 
                Buttons = cardButtons
            };

            return thumbnailCard.ToAttachment();
        }
       
        static void BuildReplyForSite(List<ResultantTable> lstresults, Microsoft.Bot.Connector.IMessageActivity reply)
        {

            foreach (var item in lstresults)
            {
                List<CardAction> cardButtons = new List<CardAction>();
                List<CardImage> cardImages = new List<CardImage>();
                string icon = string.Empty;
                cardButtons.Add(new CardAction
                {
                    Title = "Open",
                    Value = item.Path,
                    Type = ActionTypes.OpenUrl,
                });
                cardImages.Add(new CardImage(url: icon));

                ThumbnailCard tc = new ThumbnailCard();
                tc.Title = (item.Title != null) ? item.Title : "Untitled";
                tc.Text = (item.HitHighlightedSummary != null) ? item.HitHighlightedSummary : item.Description;
                tc.Images = cardImages;
                tc.Buttons = cardButtons;
                reply.Attachments.Add(tc.ToAttachment());
            }
        }

        // Entities found in result
        public string BotEntityRecognition(LuisResult result)
        {
            StringBuilder entityResults = new StringBuilder();

            if (result.Entities.Count > 0)
            {
                foreach (EntityRecommendation item in result.Entities)
                {
                    // Query: Turn on the [light]
                    // item.Type = "HomeAutomation.Device"
                    // item.Entity = "light"
                    entityResults.Append(item.Type + "=" + item.Entity + ",");
                }
                // remove last comma
                entityResults.Remove(entityResults.Length - 1, 1);
            }

            return entityResults.ToString();
        }

        /// <summary>
        /// DisplayThumbnailCard
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task DisplayThumbnailCard(IDialogContext context, Microsoft.Bot.Connector.IMessageActivity responseMessage)
        {
            var replyMessage = context.MakeMessage();
            replyMessage.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            foreach (var item in responseMessage.Attachments)
            {
                replyMessage.Attachments.Add(item);
            }
            await context.PostAsync(replyMessage);
        }

        public async Task<string> GetAnswer(string Query)
        {
            string _KnowledgeBase = "1ead6f50-8eb9-426a-b114-2bcbee8ef6bc";
            string _OcpApimSubscriptionKey = "f845c5a1-5180-4fb2-9625-9f7a721d6556";
            string RequestURI = String.Format("{0}{1}{2}{3}{4}",
                @"https://cmshrpolicyqnasm.azurewebsites.net/", 
                @"qnamaker/",
                @"knowledgebases/",
                _KnowledgeBase,
                @"/generateAnswer");
            var httpContent =
                    new StringContent($"{{\"question\": \"{Query}\"}}",
                    Encoding.UTF8, "application/json");

            var response = await Post(RequestURI, httpContent, _OcpApimSubscriptionKey);
            var answers = JsonConvert.DeserializeObject<QnAAnswer>(response);
            if (answers.answers.Count == 1 && answers.answers[0].answer == "No good match found in KB.")
            {
                return "No good match found in FAQ.";
            }
            else if (answers.answers.Count > 0)
            {
                return answers.answers[0].answer;
            }
            else
            {
                return "No good match found in FAQ.";
            }
        }


        public async Task<string> Post(string uri, StringContent body, string endpointKey)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Post;
                request.RequestUri = new Uri(uri);
                request.Content = body;
                request.Headers.Add("Authorization", "EndpointKey " + endpointKey);

                var response = await client.SendAsync(request);
                return await response.Content.ReadAsStringAsync();

            }
        }
    }

    public class SharePointLogin
    {
        public string SiteUrl { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }

    public class ResultantTable
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileExtension { get; set; }
        public string HitHighlightedSummary { get; set; }
        public string Path { get; set; }
        public string ServerRedirectedURL { get; set; }
    }
    public class Metadata
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class Answer
    {
        public IList<string> questions { get; set; }
        public string answer { get; set; }
        public double score { get; set; }
        public int id { get; set; }
        public string source { get; set; }
        public IList<object> keywords { get; set; }
        public IList<Metadata> metadata { get; set; }
    }

    public class QnAAnswer
    {
        public IList<Answer> answers { get; set; }
    }

    /// <summary>
    /// QnAMakerService is a wrapper over the QnA Maker REST endpoint
    /// </summary>
    [Serializable]
    public class QnAMakerService
    {
        private string qnaServiceHostName;
        private string knowledgeBaseId;
        private string endpointKey;

        /// <summary>
        /// Initialize a particular endpoint with it's details
        /// </summary>
        /// <param name="hostName">Hostname of the endpoint</param>
        /// <param name="kbId">Knowledge base ID</param>
        /// <param name="ek">Endpoint Key</param>
        public QnAMakerService(string hostName, string kbId, string endpointkey)
        {
            qnaServiceHostName = hostName;
            knowledgeBaseId = kbId;
            endpointKey = endpointkey;
        }
        async Task<string> Post(string uri, string body)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Post;
                request.RequestUri = new Uri(uri);
                request.Content = new StringContent(body, Encoding.UTF8, "application/json");
                request.Headers.Add("Authorization", "EndpointKey " + endpointKey);

                var response = await client.SendAsync(request);
                return await response.Content.ReadAsStringAsync();

            }
        }
        public async Task<string> GetAnswer(string question)
        {
            string uri = qnaServiceHostName + "/qnamaker/knowledgebases/" + knowledgeBaseId + "/generateAnswer";
            string questionJSON = @"{'question': '" + question + "'}";

            var response = await Post(uri, questionJSON);

            var answers = JsonConvert.DeserializeObject<QnAAnswer>(response);
            if (answers.answers.Count > 0)
            {
                return answers.answers[0].answer;
            }
            else
            {
                return "No good match found.";
            }
        }
    }
}