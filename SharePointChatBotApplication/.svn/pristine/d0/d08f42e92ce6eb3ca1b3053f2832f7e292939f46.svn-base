using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Client.Search.Query;
using System.Linq;
using System.IO;
using Newtonsoft.Json;


namespace Microsoft.Bot.Sample.LuisBot
{
    // For more information about this template visit http://aka.ms/azurebots-csharp-luis
    [Serializable]
    public class BasicLuisDialog : LuisDialog<object>
    {
        public BasicLuisDialog() : base(new LuisService(new LuisModelAttribute(
            ConfigurationManager.AppSettings["LuisAppId"], 
            ConfigurationManager.AppSettings["LuisAPIKey"], 
            domain: ConfigurationManager.AppSettings["LuisAPIHostName"])))
        {
        }

        [LuisIntent("None"), LuisIntent("none"), LuisIntent("")]
        public async Task None(IDialogContext context, LuisResult luisResult)
            {
                await context.PostAsync($"Sorry I did not understand you.");
                context.Wait(this.MessageReceived);
            }

        [LuisIntent("SearchDocument")]
        public async Task FindDocument(IDialogContext context, LuisResult luisResult)
            {
                try
                {
                    //EntityRecommendation documentName;
                    SharePointLogin sharePointLogin = new SharePointLogin();
                    sharePointLogin.SiteUrl = ConfigurationManager.AppSettings["siteUrl"];
                    sharePointLogin.EmailAddress = ConfigurationManager.AppSettings["emailAddress"];
                    sharePointLogin.Password = ConfigurationManager.AppSettings["password"];

                    await DocumentSearch(context, luisResult, sharePointLogin);

                    //need to pass luisResult and then change the code accordingly..
                    //await SiteSearch(context, luisResult, sharePointLogin);
                }
                catch (Exception e)
                {
                    var exc = e.Message;
                }
            
            }

        [LuisIntent("Welcome")]
        public async Task WelcomeMessage(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                await context.PostAsync(String.Format("Hi, I am HubSearch Bot. I can do the following tasks."));
                await context.PostAsync(String.Format("1. Document Search \n 2. Site Search \n 3. FAQs"));                
            }
            catch (Exception e)
            {
                var exc = e.Message;
            }

        }

        [LuisIntent("NavigateSite")]
        public async Task SearchPage(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                //EntityRecommendation documentName;
                SharePointLogin sharePointLogin = new SharePointLogin();
                sharePointLogin.SiteUrl = ConfigurationManager.AppSettings["siteUrl"];
                sharePointLogin.EmailAddress = ConfigurationManager.AppSettings["emailAddress"];
                sharePointLogin.Password = ConfigurationManager.AppSettings["password"];

                //need to pass luisResult and then change the code accordingly..
                await SiteSearch(context, luisResult, sharePointLogin);
            }
            catch (Exception e)
            {
                var exc = e.Message;
            }

        }

        [LuisIntent("HRPolicy")]
        public async Task FindQnAIntent(IDialogContext context, LuisResult luisResult)
        {
            try
            {
                //Ask the CRM HR knowledge base

                var qnaMakerAnswer = await GetAnswer(luisResult.Query);
                await context.PostAsync($"{qnaMakerAnswer}");
                context.Wait(MessageReceived);

            }
            catch (Exception e)
            {
                var exc = e.Message;
            }

        }

        private async Task DocumentSearch(IDialogContext context, LuisResult luisResult, SharePointLogin spLogin)
            {
                Microsoft.Bot.Connector.IMessageActivity responseMessage = Activity.CreateMessageActivity();

                ClientContext clientContext = new ClientContext(spLogin.SiteUrl);
                clientContext.Credentials = new NetworkCredential(spLogin.EmailAddress, spLogin.Password);
                clientContext.AuthenticationMode = ClientAuthenticationMode.Default;
                clientContext.ExecuteQuery();

                var x = luisResult;

                if (luisResult.Entities.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(string.Format("{0}:\"{1}\"", luisResult.Entities[0].Type, luisResult.Entities[0].Entity));

                    for (int i = 1; i < luisResult.Entities.Count; i++)
                    {

                        ///Industry: Financial Services DocumentType: Case Study

                        ///Industry: Financial Services AND DocumentType: Case Study

                        ///DocumentType: "Education Materials" OR DocumentType : "Roadmap"

                        ///DocumentType: "GTM Presentations" NOT Industry : "Healthcare"
                        ///

                        string validOperator = (luisResult.Entities[i - 1].Type == luisResult.Entities[i].Type) ? " OR " : " AND ";
                        sb.Append(validOperator);
                        //sb.Append(" NOT ");
                        sb.Append(string.Format("{0}:\"{1}\"", luisResult.Entities[i].Type, luisResult.Entities[i].Entity));

                    }

                    string keyvalue = sb.ToString();

                    KeywordQuery keywordQuery = new KeywordQuery(clientContext);
                    keywordQuery.QueryText = string.Concat(keyvalue.ToString(), " IsDocument:1");
                    keywordQuery.RowLimit = 10;
                    SearchExecutor searchExecutor = new SearchExecutor(clientContext);

                    ClientResult<ResultTableCollection> results = searchExecutor.ExecuteQuery(keywordQuery);
                    clientContext.ExecuteQuery();

                    /// start testing sample code //
                    // get recognized entities
                    string entities = this.BotEntityRecognition(luisResult);

                    // round number
                    string roundedScore = luisResult.Intents[0].Score != null ? (Math.Round(luisResult.Intents[0].Score.Value, 2).ToString()) : "0";

                    //await context.PostAsync($"**Query**: {luisResult.Query}, **Intent**: {luisResult.Intents[0].Intent}, **Score**: {roundedScore}. **Entities**: {entities}");
                    context.Wait(MessageReceived);

                    //// end testing code ///

                    if (results.Value != null && results.Value.Count > 0 && results.Value[0].RowCount > 0)
                    {
                        responseMessage.Text += "I found some potential interesting reading for you!";
                        BuildReply(results, responseMessage);
                    }
                    else
                    {
                        Boolean QueryTransformed = true;
                        if (QueryTransformed)
                        {
                            //fallback with the original message
                            keywordQuery.QueryText = string.Concat(keyvalue.Replace("?", ""), " IsDocument:1");
                            keywordQuery.RowLimit = 3;
                            searchExecutor = new SearchExecutor(clientContext);
                            results = searchExecutor.ExecuteQuery(keywordQuery);
                            clientContext.ExecuteQuery();
                            if (results.Value != null && results.Value.Count > 0 && results.Value[0].RowCount > 0)
                            {
                                responseMessage.Text += "I found some potential interesting reading for you!";
                                BuildReply(results, responseMessage);
                            }
                            else
                                responseMessage.Text += "I could not find any interesting document!";
                        }
                        else
                            responseMessage.Text += "I could not find any interesting document!";
                    }
                }

            // round number
            Double roundedScoreNew = luisResult.Intents[0].Score != null ? Math.Round(luisResult.Intents[0].Score.Value, 2) : 0.0;

            if (responseMessage.Attachments.Count < 1 && roundedScoreNew < 0.81)
            {
                await context.PostAsync(String.Format("Sorry I did not understand you."));
            }
            else
            {
                await context.PostAsync(String.Format("I found {0} documents matching your criteria.", responseMessage.Attachments.Count));
                await this.DisplayThumbnailCard(context, responseMessage);
            }

        }

        private async Task SiteSearch(IDialogContext context, LuisResult luisResult, SharePointLogin spLogin)
        {
            Microsoft.Bot.Connector.IMessageActivity responseMessage = Activity.CreateMessageActivity();

            string sharepointHostWebUrl = spLogin.SiteUrl;

            if (luisResult.Entities.Count > 0)
            {
                //chemicals                
                string keyvalue = luisResult.Entities[0].Entity; 
                string searchRestUrl = string.Format("/_api/search/query?querytext='{0}'&querytemplate='IsDocument:false'", keyvalue);
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(sharepointHostWebUrl.ToString() + searchRestUrl);
                request.Credentials = new NetworkCredential(spLogin.EmailAddress, spLogin.Password);
                request.Method = "GET";
                request.Accept = "application/xml";
                //request.Headers.Add("Content-Type", "application/xml");
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                // process response..
                XDocument oDataXML = XDocument.Load(response.GetResponseStream(), LoadOptions.PreserveWhitespace);
                XNamespace d = "http://schemas.microsoft.com/ado/2007/08/dataservices";
                XNamespace m = "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";

                List<XElement> items = oDataXML.Descendants(d + "query")
                                         .Elements(d + "PrimaryQueryResult")
                                         .Elements(d + "RelevantResults")
                                         .Elements(d + "Table")
                                         .Elements(d + "Rows")
                                         .Elements(d + "element")
                                         .ToList();

                // N.B. there might be a more elegant/efficient way of extracting the values from the (slightly awkward) XML than this.. 
                var searchResults = (from item in items
                                     select new ResultantTable
                                     {
                                         Title = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "Title").Parent.Element(d + "Value").Value,
                                         Description = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "Description").Parent.Element(d + "Value").Value,
                                         FileExtension = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "FileExtension").Parent.Element(d + "Value").Value,
                                         HitHighlightedSummary = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "HitHighlightedSummary").Parent.Element(d + "Value").Value,
                                         Path = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "Path").Parent.Element(d + "Value").Value,
                                         ServerRedirectedURL = item.Element(d + "Cells").Descendants(d + "Key").First(a => a.Value == "ServerRedirectedURL").Parent.Element(d + "Value").Value
                                     }).ToList();

                searchResults = searchResults.Where(r => r.Title.Trim().ToLower().Contains(keyvalue.Trim().ToLower())).ToList();

                if (searchResults != null && searchResults.Count > 0)
                {
                    responseMessage.Text += "I found some potential interesting reading for you!";
                    BuildReplyForSite(searchResults, responseMessage);
                }
                await context.PostAsync(String.Format("I found {0} documents matching your criteria.", responseMessage.Attachments.Count));
                await this.DisplayThumbnailCard(context, responseMessage);
            }
        }

        static void BuildReply(ClientResult<ResultTableCollection> results, Microsoft.Bot.Connector.IMessageActivity reply)
        {
            foreach (var row in results.Value[0].ResultRows)
            {
                Microsoft.Bot.Connector.Attachment attachment = GetProfileThumbnailCard(row);

                reply.Attachments.Add(attachment);
            }
        }
        /// <summary>
        /// GetProfileThumbnailCard
        /// </summary>
        /// <returns></returns>
        private static Microsoft.Bot.Connector.Attachment GetProfileThumbnailCard(IDictionary<string, object> row)
        {
            List<CardAction> cardButtons = new List<CardAction>();
            List<CardImage> cardImages = new List<CardImage>();
            string ct = string.Empty;
            string icon = string.Empty;
            switch (row["FileExtension"].ToString())
            {
                case "docx":
                    ct = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                    icon = "https://cdn3.iconfinder.com/data/icons/logos-brands-3/24/logo_brand_brands_logos_word-32.png";
                    break;
                case "xlsx":
                    ct = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    icon = "https://cdn3.iconfinder.com/data/icons/logos-brands-3/24/logo_brand_brands_logos_excel-32.png";
                    break;
                case "pptx":
                    ct = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                    icon = "https://cdn3.iconfinder.com/data/icons/logos-brands-3/24/logo_brand_brands_logos_powerpoint-32.png";
                    break;
                case "pdf":
                    ct = "application/pdf";
                    icon = "https://cdn4.iconfinder.com/data/icons/CS5/32/ACP_PDF%202_file_document.png";
                    break;

            }
            cardButtons.Add(new CardAction
            {
                Title = "Open",
                Value = (row["ServerRedirectedURL"] != null) ? row["ServerRedirectedURL"].ToString() : row["Path"].ToString(),
                Type = ActionTypes.OpenUrl
            });
            cardImages.Add(new CardImage(url: icon));

            var thumbnailCard = new ThumbnailCard
            {
                // title of the card
                Title = (row["Title"] != null) ? row["Title"].ToString() : "Untitled",
                //Detail Text
                Text = (row["Description"] != null) ? row["Description"].ToString() : string.Empty,
                // smallThumbnailCard  Image
                Images = cardImages,
                // list of buttons 
                Buttons = cardButtons
            };
           
            return thumbnailCard.ToAttachment();
        }

        static void BuildReplyForSite(List<ResultantTable> lstresults, Microsoft.Bot.Connector.IMessageActivity reply)
        {
          
            foreach (var item in lstresults)
            {
                List<CardAction> cardButtons = new List<CardAction>();
                List<CardImage> cardImages = new List<CardImage>();
                string icon = string.Empty;
                cardButtons.Add(new CardAction
                {
                    Title = "Open",
                    Value = item.Path,
                    Type = ActionTypes.OpenUrl,
                });
                cardImages.Add(new CardImage(url: icon));

                ThumbnailCard tc = new ThumbnailCard();
                tc.Title = (item.Title != null) ? item.Title : "Untitled";
                tc.Text = (item.HitHighlightedSummary != null) ? item.HitHighlightedSummary : item.Description;
                tc.Images = cardImages;
                tc.Buttons = cardButtons;
                reply.Attachments.Add(tc.ToAttachment());
            }
        }

        // Entities found in result
        public string BotEntityRecognition(LuisResult result)
        {
            StringBuilder entityResults = new StringBuilder();

            if (result.Entities.Count > 0)
            {
                foreach (EntityRecommendation item in result.Entities)
                {
                    // Query: Turn on the [light]
                    // item.Type = "HomeAutomation.Device"
                    // item.Entity = "light"
                    entityResults.Append(item.Type + "=" + item.Entity + ",");
                }
                // remove last comma
                entityResults.Remove(entityResults.Length - 1, 1);
            }

            return entityResults.ToString();
        }

        /// <summary>
        /// DisplayThumbnailCard
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task DisplayThumbnailCard(IDialogContext context, Microsoft.Bot.Connector.IMessageActivity responseMessage)
        {
            var replyMessage = context.MakeMessage();
            replyMessage.AttachmentLayout = AttachmentLayoutTypes.Carousel;
            foreach (var item in responseMessage.Attachments)
            {
                replyMessage.Attachments.Add(item);
            }
            await context.PostAsync(replyMessage);
        }        
        
          public async Task<string> GetAnswer(string Query)
        {
            string _KnowledgeBase = "c9d031f2-8fe5-4d74-abaa-71c457069398";
            string _OcpApimSubscriptionKey = "46b8ca6f-8f67-40b3-a6eb-14cfb315e992";

            string RequestURI = String.Format("{0}{1}{2}{3}{4}",
                @"https://qnamakermscog.azurewebsites.net/",
                @"qnamaker/",
                @"knowledgebases/",
                _KnowledgeBase,
                @"/generateAnswer");
            var httpContent =
                    new StringContent($"{{\"question\": \"{Query}\"}}",
                    Encoding.UTF8, "application/json");

            var response = await Post(RequestURI, httpContent, _OcpApimSubscriptionKey);
            var answers = JsonConvert.DeserializeObject<QnAAnswer>(response);
            if (answers.answers.Count > 0)
            {
                return answers.answers[0].answer;
            }
            else
            {
                return "No good match found.";
            }
        }


        public async Task<string> Post(string uri, StringContent body, string endpointKey)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Post;
                request.RequestUri = new Uri(uri);
                request.Content = body;
                request.Headers.Add("Authorization", "EndpointKey " + endpointKey);

                var response = await client.SendAsync(request);
                return await response.Content.ReadAsStringAsync();

            }
        }
    }
    
    public class SharePointLogin
    {
        public string SiteUrl { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
    }

    public class ResultantTable
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileExtension { get; set; }        
        public string HitHighlightedSummary { get; set; }
        public string Path { get; set; }
        public string ServerRedirectedURL { get; set; }
    }
     public class Metadata
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class Answer
    {
        public IList<string> questions { get; set; }
        public string answer { get; set; }
        public double score { get; set; }
        public int id { get; set; }
        public string source { get; set; }
        public IList<object> keywords { get; set; }
        public IList<Metadata> metadata { get; set; }
    }

    public class QnAAnswer
    {
        public IList<Answer> answers { get; set; }
    }   

    /// <summary>
    /// QnAMakerService is a wrapper over the QnA Maker REST endpoint
    /// </summary>
    [Serializable]
    public class QnAMakerService
    {
        private string qnaServiceHostName;
        private string knowledgeBaseId;
        private string endpointKey;

        /// <summary>
        /// Initialize a particular endpoint with it's details
        /// </summary>
        /// <param name="hostName">Hostname of the endpoint</param>
        /// <param name="kbId">Knowledge base ID</param>
        /// <param name="ek">Endpoint Key</param>
        public QnAMakerService(string hostName, string kbId, string endpointkey)
        {
            qnaServiceHostName = hostName;
            knowledgeBaseId = kbId;
            endpointKey = endpointkey;
        }
        async Task<string> Post(string uri, string body)
        {
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage())
            {
                request.Method = HttpMethod.Post;
                request.RequestUri = new Uri(uri);
                request.Content = new StringContent(body, Encoding.UTF8, "application/json");
                request.Headers.Add("Authorization", "EndpointKey " + endpointKey);

                var response = await client.SendAsync(request);
                return await response.Content.ReadAsStringAsync();

            }
        }
        public async Task<string> GetAnswer(string question)
        {
            string uri = qnaServiceHostName + "/qnamaker/knowledgebases/" + knowledgeBaseId + "/generateAnswer";
            string questionJSON = @"{'question': '" + question + "'}";

            var response = await Post(uri, questionJSON);

            var answers = JsonConvert.DeserializeObject<QnAAnswer>(response);
            if (answers.answers.Count > 0)
            {
                return answers.answers[0].answer;
            }
            else
            {
                return "No good match found.";
            }
        }
    }
}