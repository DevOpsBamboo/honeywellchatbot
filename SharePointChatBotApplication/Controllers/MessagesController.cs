using System;
using System.Threading.Tasks;
using System.Web.Http;

using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Internals;
using System.Web.Http.Description;
using System.Net.Http;
//using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using AuthBot;
using AuthBot.Dialogs;
using Microsoft.Bot.Builder;

namespace Microsoft.Bot.Sample.LuisBot
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        /// <summary>
        /// POST: api/Messages
        /// receive a message from a user and send replies
        /// </summary>
        /// <param name="activity"></param>
        [ResponseType(typeof(void))]
        public virtual async Task<HttpResponseMessage> Post([FromBody] Activity activity)
        {
            if (activity.GetActivityType() == ActivityTypes.Message)
            {
                try
                {
                    Activity reply = activity.CreateReply();
                    reply.Type = ActivityTypes.Typing;
                    reply.Text = null;
                    ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
                    await connector.Conversations.ReplyToActivityAsync(reply);
                    await Conversation.SendAsync(activity, () => new BasicLuisDialog());
                }
                catch(Exception e)
                {
                    var error = e.Message;
                }
            }
            else
            {
                await HandleSystemMessage(activity);
            }
            return new HttpResponseMessage(System.Net.HttpStatusCode.Accepted);
        }

        private async Task<Activity> HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
                if (message.MembersAdded.Any(o => o.Id == message.Recipient.Id))
                {
                    //ConnectorClient client = new ConnectorClient(new System.Uri(message.ServiceUrl));

                    //var reply = message.CreateReply();
                    ////StringBuilder response = new StringBuilder();
                    //////response.Append("Hi! I’m the Hub Helper Bot. Please login with your windows credentials");
                    ////response.Append("<br>Click <a href='Google.com' >here</a> to login");
                    ////reply.Text = response.ToString();

                    //List<CardAction> cardButtons = new List<CardAction>();
                    //List<CardImage> cardImages = new List<CardImage>();
                    //string icon = string.Empty;
                    //cardButtons.Add(new CardAction
                    //{
                    //    Title = "Login",
                    //    Value = "http://localhost:3984/WebForm1.aspx",
                    //    Type = ActionTypes.OpenUrl,
                    //});
                    //cardImages.Add(new CardImage(url: icon));

                    //ThumbnailCard tc = new ThumbnailCard();
                    //tc.Title = "Hi.. Please Login with your windows credentials.";
                    //tc.Images = cardImages;
                    //tc.Buttons = cardButtons;
                    //reply.Attachments.Add(tc.ToAttachment());             

                    //await client.Conversations.ReplyToActivityAsync(reply);
                }

            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Event)
            {
                if (message.IsTokenResponseEvent())
                {
                    //await Conversation.SendAsync(message, () => new Dialogs.RootDialog());
                    await Conversation.SendAsync(message, () => new BasicLuisDialog());
                    //await Conversation.SendAsync(message, () => new DialogContext());
                    //await Conversation.SendAsync(message, () => new);

                }
            }
            return null;
        }

    }
}