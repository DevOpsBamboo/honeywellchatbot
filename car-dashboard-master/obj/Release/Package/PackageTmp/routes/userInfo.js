
var config = require('./config.js');
var userService = config.Server + "Userinfo.svc";
// JavaScript source code
function getUserInformation(userId, callback) {
    var BasicHttpBinding = require('wcf.js').BasicHttpBinding
        , Proxy = require('wcf.js').Proxy
        , binding = new BasicHttpBinding()
        , proxy = new Proxy(binding, userService)
        , message = '<Envelope xmlns=' +
            '"http://schemas.xmlsoap.org/soap/envelope/">' +
            '<Header />' +
            '<Body>' +
			'<GetUserProfile xmlns="http://tempuri.org/">' +
			'<username>' + userId + '</username>' +
            '</GetUserProfile>' +
            '</Body>' +
            '</Envelope>'
    proxy.send(message, "http://tempuri.org/IUserInfo/GetUserProfile", function (response, ctx) {
        console.log(response);

        var parseString = require('xml2js').parseString;
        parseString(response, function (err, result) {
            callback(null, result);
            return;
        });
    });
}

function authenticateUser(userName, password, callback) {
    var BasicHttpBinding = require('wcf.js').BasicHttpBinding
        , Proxy = require('wcf.js').Proxy
        , binding = new BasicHttpBinding()
        , proxy = new Proxy(binding, userService)
        , message = '<Envelope xmlns=' +
            '"http://schemas.xmlsoap.org/soap/envelope/">' +
            '<Header />' +
            '<Body>' +
			'<CheckUserLogin xmlns="http://tempuri.org/">' +
			'<username>' + userName + '</username>' +
			'<password>' + password + '</password>' +
            '</CheckUserLogin>' +
            '</Body>' +
            '</Envelope>'
    proxy.send(message, "http://tempuri.org/IUserInfo/CheckUserLogin", function (response, ctx) {
        console.log(response);

        var parseString = require('xml2js').parseString;
        parseString(response, function (err, result) {
            callback(null, result);
            return;
        });
    });
}

function registerUser(name,email,password,callback) {
    var BasicHttpBinding = require('wcf.js').BasicHttpBinding
        , Proxy = require('wcf.js').Proxy
        , binding = new BasicHttpBinding()
        , proxy = new Proxy(binding, userService)
        , message = '<Envelope xmlns=' +
            '"http://schemas.xmlsoap.org/soap/envelope/">' +
            '<Header />' +
            '<Body>' +
            '<Register xmlns="http://tempuri.org/">' +
            '<UserName>' + name + '</UserName>' +
            '<Email>' + email + '</Email>' +
            '<pwd>' + password + '</pwd>' +
            '</Register>' +
            '</Body>' +
            '</Envelope>'
    proxy.send(message, "http://tempuri.org/IUserInfo/Register", function (response, ctx) {
        //console.log(response);
        var parseString = require('xml2js').parseString;
        parseString(response, function (err, result) {
            callback(null, result);
            return;
        });
    });
}

function updateProfile(userID, premium, deductible, medicalEvents, tobacco, bloodPressure, cardiacArrest, pregnancy, breastCancer, callback) {
	var BasicHttpBinding = require('wcf.js').BasicHttpBinding
		, Proxy = require('wcf.js').Proxy
        , binding = new BasicHttpBinding()
        , proxy = new Proxy(binding, userService)
		, message = '<Envelope xmlns=' +
			'"http://schemas.xmlsoap.org/soap/envelope/">' +
			'<Header />' +
			'<Body>' +
			'<UpdateProfile xmlns="http://tempuri.org/">' +
			'<username>' + userID + '</username>' +
			'<premium>' + premium + '</premium>' +
			'<deductible>' + deductible + '</deductible>' +
			'<medicalEvents>' + medicalEvents + '</medicalEvents>' +
			'<tobacco>' + tobacco + '</tobacco>' +
			'<bloodPressure>' + bloodPressure + '</bloodPressure>' +
			'<cardiacArrest>' + cardiacArrest + '</cardiacArrest>' +
			'<pregnancy>' + pregnancy + '</pregnancy>' +
			'<breastCancer>' + breastCancer + '</breastCancer>' +
			'</UpdateProfile>' +
			'</Body>' +
			'</Envelope>'
	proxy.send(message, "http://tempuri.org/IUserInfo/UpdateProfile", function (response, ctx) {
		//console.log(response);
		var parseString = require('xml2js').parseString;
		parseString(response, function (err, result) {
			callback(null, result);
			return;
		});
	});
}

exports.registerUser = registerUser;
exports.getUserInformation = getUserInformation;
exports.authenticateUser = authenticateUser;
exports.updateProfile = updateProfile;

