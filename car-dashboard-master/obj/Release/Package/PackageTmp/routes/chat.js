const path = require('path');
var express = require('express');
var router = express.Router();
var userinfo = require('./userInfo.js');
var login = require('./login.js');
var profile = [];

//router.get('/login', function (req, res) {
//	res.sendFile(path.resolve(__dirname, '../ui/login.html'));
//});

router.post('/chat', function (req, res) {
	userinfo.getUserInformation(req.body.username, function (err, response) {
				if (err) {
					console.log(err);
				} else {
					console.log("output" + JSON.stringify(response, null, 2));
					router.profile = response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0];
                    res.render('../ui/chat.html', {
                        username: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:Username'][0],
					});
					console.log(profile);
				}
			});
});

module.exports = router;
