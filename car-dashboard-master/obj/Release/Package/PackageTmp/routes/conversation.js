/**
 * Copyright 2016 IBM Corp. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const watson = require('watson-developer-cloud'); // watson sdk
const benefitsplanservice = require('./benefitplanservice.js');
const chat = require('./chat.js');
const userinfo = require('./userInfo.js');
var objresult;
var deductible = 0;
var VRResponse = {};
var benefitPlans = [];
const formatCurrency = require('format-currency');
var opts = { format: '%s%v', symbol: '$', maximumFractionDigits: 0, minimumFractionDigits:0 };

// Create the service wrapper
const conversation = watson.conversation({
	// If unspecified here, the CONVERSATION_USERNAME and CONVERSATION_PASSWORD env properties will be checked
	// After that, the SDK will fall back to the bluemix-provided VCAP_SERVICES environment property
	// username: '<username>',
	// password: '<password>',
	version_date: '2016-10-21',
	version: 'v1'
});

/**
 * Updates the response text using the intent confidence
 * @param  {Object} input The request to the Conversation service
 * @param  {Object} response The response from the Conversation service
 * @return {Object}          The response with the updated message
 */


//	// if (response.intents && response.intents[0]) {
//	//   var intent = response.intents[0];
//	//   // Depending on the confidence of the response the app can return different messages.
//	//   // The confidence will vary depending on how well the system is trained. The service will always try to assign
//	//   // a class/intent to the input. If the confidence is low, then it suggests the service is unsure of the
//	//   // user's intent . In these cases it is usually best to return a disambiguation message
//	//   // ('I did not understand your intent, please rephrase your question', etc..)
//	//   if (intent.confidence >= 0.75) {
//	//     responseText = 'I understood your intent was ' + intent.intent;
//	//   } else if (intent.confidence >= 0.5) {
//	//     responseText = 'I think your intent was ' + intent.intent;
//	//   } else {
//	//     responseText = 'I did not understand your intent';
//	//   }
//	// }
//	//response.output.text = responseText;
//	//callback(response);};



const updateMessage = (input, response, callback) => {

	var userProfile = chat.profile;

	var responseObject = response;

	if (response.context.PlanCategory == "yes") {
		response.context.PlanCategory = "no";
		callback(response);
	}

	else if (response.context.PlanSelect == "yes") {
		response.context.PlanSelect = "no";
		callback(response);
	}

	else if (response.context.StartOver == true)
	{
		getPlans(userProfile['a:Country'][0], userProfile['a:State'][0], userProfile['a:City'][0]);
	}

	else if (response.context.system.dialog_turn_counter == 1) {
		getPlans(userProfile['a:Country'][0], userProfile['a:State'][0], userProfile['a:City'][0]);
		callback(response);
	}

	else if (response.output == null) {
		response.output = {};
		callback(response);
	}

	else if (response.output.nodes_visited[0] == "anything_else" || response.context.AnythingElse == "-1") {
		response.context.AnythingElse = "none";
		callback(response);
	}

	else if (response.context.Premium == "-1") {

				userProfile['a:Premium'][0] = "0";

				userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
					userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {

						responseObject.context.Premium = 0;
						benefitPlans.sort();
						callback(responseObject);
					});

	}

	else if (response.context.Premium > 0) {

					userProfile['a:Premium'][0] = response.context.Premium;

					userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
						userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {

									var n = 0;

									for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
										if (benefitPlans[i]['a:Premium'][0] <= responseObject.context.Premium) {
											benefitPlans[n] = benefitPlans[i];
											n++;
										}
									}

									//if (n == 0) {
									//	responseObject.output.text[0] = 'There are no plans matching your preferences.' + ' <br/> ';
									//	callback(responseObject);
									//}

									//else {

										benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
										benefitPlans.sort();

										responseObject.context.Premium = 0;

										callback(responseObject);
									//}
						});
	}

	else if (response.context.Deductible > 0) {

				userProfile['a:Deductible'][0] = response.context.Deductible;

				userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
					userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {

						var n = 0;

						for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
							if (benefitPlans[i]['a:Deductable'][0] <= responseObject.context.Deductible) {
								benefitPlans[n] = benefitPlans[i];
								n++;
							}
						}

						
							benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
							benefitPlans.sort();

							responseObject.context.Deductible = 0;

							callback(responseObject);
						//}

					});

	}
	
	else if (response.context.MedicalEvents != "none") {

				userProfile['a:IsMajorMedicalEvent'][0] = response.context.MedicalEvents;

				userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
					userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {


						if (responseObject.context.MedicalEvents == "yes") {
							var sum = 0;
							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								sum = parseInt(sum) + parseInt(benefitPlans[i]['a:Deductable'][0]);
							}

							var average = (sum / Object.keys(benefitPlans).length);

							var n = 0;

							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								if (parseInt(benefitPlans[i]['a:Deductable'][0]) <= average) {
									benefitPlans[n] = benefitPlans[i];
									n++;
								}
							}					


								benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
								benefitPlans.sort();

								responseObject.context.MedicalEvents = "none";

								callback(responseObject);
						
						}
						else if (responseObject.context.MedicalEvents == "no") {
							responseObject.context.MedicalEvents = "none";
							callback(responseObject);
						}

					});
	}

	else if (response.context.Tobacco != "none") {

				userProfile['a:IsSmoking'][0] = response.context.Tobacco;

				userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
					userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {

						if (responseObject.context.Tobacco == "yes") {
							var sumXRay = 0;
							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								sumXRay = parseInt(sumXRay) + parseInt(benefitPlans[i]['a:XRay'][0]);
							}

							var averageXRay = (sumXRay / Object.keys(benefitPlans).length);

							var sumDoctor = 0;
							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								sumDoctor = parseInt(sumDoctor) + parseInt(benefitPlans[i]['a:DoctorVisit'][0]);
							}

							var averageDoctor = (sumDoctor / Object.keys(benefitPlans).length);

							var n = 0;

							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								if (parseInt(benefitPlans[i]['a:XRay'][0]) <= averageXRay && parseInt(benefitPlans[i]['a:DoctorVisit'][0]) <= averageDoctor) {
									benefitPlans[n] = benefitPlans[i];
									n++;
								}
							}
								benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
								benefitPlans.sort();


								responseObject.context.Tobacco = "none";

								callback(responseObject);
						}
						else if (responseObject.context.Tobacco == "no") {
							responseObject.context.Tobacco = "none";
							callback(responseObject);
						}

					});
	}

	else if (response.context.BloodPressure != "none") {

				userProfile['a:BloodPressure'][0] = response.context.BloodPressure;

				userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
					userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {

						if (responseObject.context.BloodPressure == "yes") {
							var sumMedication = 0;
							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								sumMedication = parseInt(sumMedication) + parseInt(benefitPlans[i]['a:Medication'][0]);
							}

							var averageMedication = (sumMedication / Object.keys(benefitPlans).length);


							var n = 0;

							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								if (parseInt(benefitPlans[i]['a:Medication'][0]) <= averageMedication) {
									benefitPlans[n] = benefitPlans[i];
									n++;
								}
							}

							
								benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
								benefitPlans.sort();

								responseObject.context.BloodPressure = "none";

								callback(responseObject);
						}
						else if (responseObject.context.BloodPressure == "no") {
							responseObject.context.BloodPressure = "none";
							callback(responseObject);
						}
					});
	}

	else if (response.context.CardiacArrest != "none") {

				userProfile['a:CardiacArrest'][0] = response.context.CardiacArrest;

				userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
					userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {

						if (responseObject.context.CardiacArrest == "yes") {

							var sumDoctorVisit = 0;
							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								sumDoctorVisit = parseInt(sumDoctorVisit) + parseInt(benefitPlans[i]['a:DoctorVisit'][0]);
							}

							var averageDoctorVisit = (sumDoctorVisit / Object.keys(benefitPlans).length);

							var sumLab = 0;
							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								sumLab = parseInt(sumLab) + parseInt(benefitPlans[i]['a:LabProcedure'][0]);
							}

							var averageLab = (sumLab / Object.keys(benefitPlans).length);


							var n = 0;

							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								if (parseInt(benefitPlans[i]['a:DoctorVisit'][0]) <= averageDoctorVisit && parseInt(benefitPlans[i]['a:LabProcedure'][0]) <= averageLab) {
									benefitPlans[n] = benefitPlans[i];
									n++;
								}
							}

							
								benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
								benefitPlans.sort();


								responseObject.context.CardiacArrest = "none";

								callback(responseObject);
						}
						else if (responseObject.context.CardiacArrest == "no") {
							responseObject.context.CardiacArrest = "none";
							callback(responseObject);
						}

					});
			}

    else if (response.context.Pregnancy != "none") {

				userProfile['a:IsExpectingMother'][0] = response.context.Pregnancy;

				userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
					userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {

						if (responseObject.context.Pregnancy == "yes") {

							var sumGynecology = 0;
							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								sumGynecology = parseInt(sumGynecology) + parseInt(benefitPlans[i]['a:Gynecology'][0]);
							}

							var averageGynecology = (sumGynecology / Object.keys(benefitPlans).length);


							var n = 0;

							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								if (parseInt(benefitPlans[i]['a:Gynecology'][0]) <= averageGynecology) {
									benefitPlans[n] = benefitPlans[i];
									n++;
								}
							}

							
								benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
								benefitPlans.sort();


								responseObject.context.Pregnancy = "none";

								callback(responseObject);
						}
						else if (responseObject.context.Pregnancy == "no") {
							responseObject.context.Pregnancy = "none";
							callback(responseObject);
						}

					});
    }

    else if (response.context.BreastCancer != "none") {

				userProfile['a:IsCancerTreatmentPlanned'][0] = response.context.BreastCancer;

				userinfo.updateProfile(userProfile['a:Username'][0], userProfile['a:Premium'][0], userProfile['a:Deductible'][0], userProfile['a:IsMajorMedicalEvent'][0], userProfile['a:IsSmoking'][0],
					userProfile['a:BloodPressure'][0], userProfile['a:CardiacArrest'][0], userProfile['a:IsExpectingMother'][0], userProfile['a:IsCancerTreatmentPlanned'][0], function (err, response) {

						if (responseObject.context.BreastCancer == "yes") {


							var sumBreastCancer = 0;
							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								sumBreastCancer = parseInt(sumBreastCancer) + parseInt(benefitPlans[i]['a:Mammogram'][0]);
							}

							var averageBreastCancer = (sumBreastCancer / Object.keys(benefitPlans).length);


							var n = 0;

							for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
								if (parseInt(benefitPlans[i]['a:Mammogram'][0]) <= averageBreastCancer) {
									benefitPlans[n] = benefitPlans[i];
									n++;
								}
							}	

							
								benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
								benefitPlans.sort();

								responseObject.context.BreastCancer = "none";

								callback(responseObject);
						}

						else if (responseObject.context.BreastCancer == "no") {

							responseObject.context.BreastCancer = "none";
							callback(responseObject);
						}

					});
	}

	else if (response.context.PlanType != false)
	{
		var n = 0;

		for (var i = 0; i < Object.keys(benefitPlans).length; i++) {
			if (benefitPlans[i]['a:PlanType'][0] == response.context.PlanType) {
				benefitPlans[n] = benefitPlans[i];
				n++;
			}
		}

		if (n > 0) {

			benefitPlans.splice(n, (Object.keys(benefitPlans).length - n));
			benefitPlans.sort();

			response.context.PlanType = false;

			var array = typeof benefitPlans != 'object' ? JSON.parse(benefitPlans) : new Array(benefitPlans);

			var str = '<table cellpadding="8" class="ptable">';

			str += '<thead class="pthead"><tr>';
			str += '<th>' + 'Plan Name' + '</th>' + '<th>' + 'Premium' + '</th>' + '<th>' + 'Deductible' + '</th>';
			//+ '<th>' + 'Medication Co-Pay' + '</th>' + '<th>' + 'Doc Visit Co-Pay' + '</th>';
			str += '</tr></thead>';

			str += '<tbody>';

			if (n > 5) {
				n = 5;
			}

			for (var i = 0; i < n; i++) {
				str += (i % 2 == 0) ? '<tr>' : '<tr >';
				if (i % 2 == 0)
					str += '<tr class="ptr">';
				else
					str += '<tr >';
				str += '<td>' + array[0][i]['a:PlanName'][0] + '</td>' + '<td style="text-align: center">' + formatCurrency(array[0][i]['a:Premium'][0], opts) + '</td>' +
					'<td style="text-align: center">' + formatCurrency(array[0][i]['a:Deductable'][0], opts) + '</td>' +
					// + '<td style="text-align: center">' + formatCurrency(array[0][i]['a:Medication'][0], opts) + '</td>' +
					//'<td style="text-align: center">' + formatCurrency(array[0][i]['a:DoctorVisit'][0], opts) + '</td>' +
					'</tr>';
				str += '</tr>';
			}

			str += '</tbody>'
			str += '</table>';

			var responseWithCoPayAndDeductible = str;
			responseObject.output.text[0] += responseWithCoPayAndDeductible;

			callback(responseObject);
		}
		else {
			responseObject.output.text[0] = 'There are no plans matching your preferences';
			callback(responseObject);
		}
		
	}

    else if (response.context.UserStop == "1")
    {
        response.context.UserStop = "0";

        var array = typeof benefitPlans != 'object' ? JSON.parse(benefitPlans) : new Array(benefitPlans);

        var str = '<table cellpadding="8" class="ptable">';

        str += '<thead class="pthead"><tr>';
        str += '<th>' + 'Plan Name' + '</th>' + '<th>' + 'Premium' + '</th>' + '<th>' + 'Deductible' + '</th>';
        //+ '<th>' + 'Medication Co-Pay' + '</th>' + '<th>' + 'Doc Visit Co-Pay' + '</th>';
        str += '</tr></thead>';

        str += '<tbody>';

        for (var i = 0; i < 5; i++) {
            str += (i % 2 == 0) ? '<tr>' : '<tr >';
            if (i % 2 == 0)
                str += '<tr class="ptr">';
            else
                str += '<tr >';
            str += '<td>' + array[0][i]['a:PlanName'][0] + '</td>' + '<td style="text-align: center">' + formatCurrency(array[0][i]['a:Premium'][0], opts) + '</td>' +
                '<td style="text-align: center">' + formatCurrency(array[0][i]['a:Deductable'][0], opts) + '</td>' +
                // + '<td style="text-align: center">' + formatCurrency(array[0][i]['a:Medication'][0], opts) + '</td>' +
                //'<td style="text-align: center">' + formatCurrency(array[0][i]['a:DoctorVisit'][0], opts) + '</td>' +
                '</tr>';
            str += '</tr>';
        }

        str += '</tbody>'
        str += '</table>';

        var responseWithCoPayAndDeductible = ' <br/> ' + ' <br/> ' + str + ' <br/> ';
        responseObject.output.text[0] += responseWithCoPayAndDeductible;
        callback(responseObject);

    }

			else if (response.context.Choice != "0") {

				var selection = [];
				selection[0] = benefitPlans[parseInt(response.context.Choice) - 1];

				response.context.Choice = "0";
				
				var responseWithCoPayAndDeductible = ' You have selected: ' + ' <b> ' + selection[0]['a:PlanName'][0] + ' </b> ' + ' <br/> ' + ' <br/> ';
				responseObject.output.text[0] = responseWithCoPayAndDeductible + responseObject.output.text[0];
				callback(responseObject);

			}

			else if (response.context.Upsell == "-1") {
				response.context.Upsell = "0";
				callback(responseObject);
			}

			else if (response.context.Recommendation == "-1") {
				response.context.Recommendation = "0";
				callback(responseObject);
			}

			else if (response.context.Upsell != "0") {

				response.context.Upsell = "0";
				callback(responseObject);

			}

			else if (response.context.Recommendation != "0") {				

				response.context.Recommendation = "0";
				callback(responseObject);

			}

			else if (response.context.NotKnown == "-1") {
				response.context.NotKnown = "0";
				callback(response);
			}

			

	else if (response.context.Premium > 0 || response.context.Premium == "-1" || response.context.Deductible > 0 || response.context.PlanType > 0
		|| response.context.MedicalEvents != "none" || response.context.Tobacco != "none" || response.context.BloodPressure != "none"
		|| response.context.CardiacArrest != "none" || response.context.Pregnancy != "none" || response.context.BreastCancer != "none") {

		//do nothing
	}

			else {
				callback(response);
			}
};


function getPlans(country, state, city) {

	benefitsplanservice.GetBenefitPlansbyLoc(country, state, city, function (err, bpResponse) {

		benefitPlans = bpResponse['s:Envelope']['s:Body'][0].GetBenefitPlansbyLocResponse[0].GetBenefitPlansbyLocResult[0]['a:BenefitPlanDto'];

	});
}



module.exports = function (app) {
	app.post('/api/message', (req, res, next) => {
		const workspace = process.env.WORKSPACE_ID || '<workspace-id>';
		if (!workspace || workspace === '<workspace-id>') {
			return res.json({
				output: {
					text: 'The app has not been configured with a <b>WORKSPACE_ID</b> environment variable. Please refer to the ' +
					'<a href="https://github.com/watson-developer-cloud/conversation-simple">README</a> ' +
					'documentation on how to set this variable. <br>' +
					'Once a workspace has been defined the intents may be imported from ' +
					'<a href="https://github.com/watson-developer-cloud/conversation-simple/blob/master/training/car_workspace.json">here</a> ' +
					'in order to get a working application.'
				}
			});
		}
		const payload = {
			workspace_id: workspace,
			context: req.body.context || {},
			input: req.body.input || {}
		};

		// Send the input to the conversation service
		conversation.message(payload, (error, data) => {
			if (error) {
				return next(error);
			}
			updateMessage(payload, data, function (response) {
				if (response) {
					return res.json(response);
				}
			});

		});
	});
};
