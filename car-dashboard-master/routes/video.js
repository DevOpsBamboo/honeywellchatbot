const path = require('path');
var express = require('express');
var router = express.Router();
var userinfo = require('./userInfo.js');
var login = require('./login.js');
var video = require('./video.js');
var config = require('./config.js');
const gulp = require('gulp');

var userService = config.Server + "Userinfo.svc";


router.post('/video', function (req, res) {
    res.render('../ui/video.html', {
        emailId: req.body.username,
        name: req.body.name,
        showMessage: '',
        display: '',
        info:''
});  
});


router.post('/updateProfile', function (req, res) {
    var fs = require('fs');
    var ageMin = '0';
    var ageMax = '100';
    var gender = 'M';
    var image = req.body.imageData;
    var userName=req.body.userName+'.jpg';
    const path = './Public/Profiles/';
    var imgpath = path + userName;
    //image = "data:image/jpeg;base64," + image;
    // strip off the data: url prefix to get just the base64-encoded bytes
    //var data = image.replace(/^data:image\/\w+;base64,/, "");
    var buf = new Buffer(image, 'base64');
    fs.writeFileSync(imgpath, buf)
    

   var emailId = req.body.emailId;
    //var file_path = process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'] + "\\Downloads\\test.jpg";
    //var photoURL = req.body.photoURL;
    //var photoURL = file_path;

    var watson = require('watson-developer-cloud');

    var visual_recognition = watson.visual_recognition({ api_key: '7d5d04972fc0ede90aceb6bdd6f11c5dc5dfdacb', version: 'v3', version_date: '2018-03-19' });
    
    var params = { images_file: fs.createReadStream(imgpath) };
    //var params = { images_file: image };

 
    visual_recognition.detectFaces(params, function (err, response) {
        if (err) console.log(err);
        else {
            console.log(JSON.stringify(response, null, 2));
            if (response.images[0].faces[0] != null) {
                ageMin = response.images[0].faces[0].age.min;
                ageMax = response.images[0].faces[0].age.max;
                gender = response.images[0].faces[0].gender.gender;
                var ageString = ageMin + '-' + ageMax;
				updateAgeGender(req.body.userName, ageString, gender, function (err, result) {
                    if (err) {
                        console.log("update failed");
                    }
                    res.render('../ui/video.html', { emailId: emailId, info: "Profile Updated Successfully.", display: "success", showMessage: "show" });
                });
            }
        }
    });

});



function updateAgeGender(userName, ageRange, gender, callback) {


    var BasicHttpBinding = require('wcf.js').BasicHttpBinding
        , Proxy = require('wcf.js').Proxy
        , binding = new BasicHttpBinding()
        , proxy = new Proxy(binding, userService)
        , message = '<Envelope xmlns=' +
            '"http://schemas.xmlsoap.org/soap/envelope/">' +
            '<Header />' +
            '<Body>' +
            '<UpdateAgeGender xmlns="http://tempuri.org/">' +
			'<username>' + userName + '</username>' +
            '<ageRange>' + ageRange + '</ageRange>' +
            '<gender>' + gender + '</gender>' +
            '</UpdateAgeGender>' +
            '</Body>' +
            '</Envelope>'
    proxy.send(message, "http://tempuri.org/IUserInfo/UpdateAgeGender", function (response, ctx) {
        console.log(response);

        var parseString = require('xml2js').parseString;
        parseString(response, function (err, result) {
            callback(err, result);
        });    
    });
}


router.post('/UpdateLocInfo', function (req, res) {

    var emailId = req.body.emailId;
    var country = req.body.country;
    var state = req.body.state;
    var city = req.body.city;
	UpdateLocInfo(emailId, country, state, city, function (err, result) {
        if (err) {

            console.log("Location Information Update failed");
        }
        else {
            console.log("Location Information Updated");
        }

    });

});

function UpdateLocInfo(username, country, state, city, callback) {
    var BasicHttpBinding = require('wcf.js').BasicHttpBinding
        , Proxy = require('wcf.js').Proxy
        , binding = new BasicHttpBinding()
        , proxy = new Proxy(binding, userService)
        , message = '<Envelope xmlns=' +
            '"http://schemas.xmlsoap.org/soap/envelope/">' +
            '<Header />' +
            '<Body>' +
			'<UpdateLocInfo xmlns="http://tempuri.org/">' +
			'<username>' + username + '</username>' +
            '<country>' + country + '</country>' +
            '<state>' + state + '</state>' +
            '<city>' + city + '</city>' +
            '</UpdateLocInfo>' +
            '</Body>' +
            '</Envelope>'
    proxy.send(message, "http://tempuri.org/IUserInfo/UpdateLocInfo", function (response, ctx) {
        console.log(response);

        var parseString = require('xml2js').parseString;
        parseString(response, function (err, result) {
            callback(err, result);
        });
    });
}


module.exports = router;