﻿var config = require('./config.js');
var benefitService = config.Server + "BenfitPlanService.svc";


function getAllBenefitPlans(callback) {	
	var BasicHttpBinding = require('wcf.js').BasicHttpBinding
		, Proxy = require('wcf.js').Proxy
        , binding = new BasicHttpBinding()
        , proxy = new Proxy(binding, benefitService)
		, message = '<Envelope xmlns=' +
			'"http://schemas.xmlsoap.org/soap/envelope/">' +
			'<Header />' +
			'<Body>' +
			'<GetAllBenefitPlans xmlns="http://tempuri.org/">' +
			'</GetAllBenefitPlans>' +
			'</Body>' +
			'</Envelope>'
	proxy.send(message, "http://tempuri.org/IBenfitPlanService/GetAllBenefitPlans", function (response, ctx) {
		var parseString = require('xml2js').parseString;
		parseString(response, function (err, result) {
			callback(null, result);
			return;
		});
	});
}

function GetBenefitPlansbyLoc(country, state, city, callback) {
	var BasicHttpBinding = require('wcf.js').BasicHttpBinding
		, Proxy = require('wcf.js').Proxy
		, binding = new BasicHttpBinding()
        , proxy = new Proxy(binding, benefitService)
		, message = '<Envelope xmlns=' +
			'"http://schemas.xmlsoap.org/soap/envelope/">' +
			'<Header />' +
			'<Body>' +
			'<GetBenefitPlansbyLoc xmlns="http://tempuri.org/">' +
			'<Country>' + country + '</Country>' +
			'<State>' + state + '</State>' +
			'<City>' + city + '</City>' +
			'</GetBenefitPlansbyLoc>' +
			'</Body>' +
			'</Envelope>'
	proxy.send(message, "http://tempuri.org/IBenfitPlanService/GetBenefitPlansbyLoc", function (response, ctx) {
		var parseString = require('xml2js').parseString;
		parseString(response, function (err, result) {
			callback(null, result);
			return;
		});
	});
}

exports.getAllBenefitPlans = getAllBenefitPlans;
exports.GetBenefitPlansbyLoc = GetBenefitPlansbyLoc;


