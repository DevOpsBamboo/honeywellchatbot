const path = require('path');
var express = require('express');
var router = express.Router();
var userinfo = require('./userInfo.js');
const formatCurrency = require('format-currency');

router.get('/login', function(req, res) {
    res.sendFile(path.resolve(__dirname,'../ui/login.html'));
});

router.post('/authenticateUser', function(req, res) {
	userinfo.authenticateUser(req.body.username, req.body.password, function (err, result) {
		console.log(result);
		  if (err) {
                console.log("error" + err);
            } else {
              console.log("output" + JSON.stringify(result, null, 2));

              if (result["s:Envelope"]["s:Body"][0].CheckUserLoginResponse[0].CheckUserLoginResult[0]<=0)
              {
                  res.render('../ui/login.html', { info: "Invalid Login Details", display: "danger", showMessage: "show" });
              }
              else
              {
                  userinfo.getUserInformation(req.body.username, function (err, response) {

                      if (err) {
                          console.log(err);
                      }
                      else {
						  //console.log("output" + JSON.stringify(response, null, 2));	
							  
						  var opts = { format: '%s%v', symbol: '$', maximumFractionDigits: 0, minimumFractionDigits: 0 };

							  var premiumAmount = formatCurrency(response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:Premium'][0], opts);
							  var deductibleAmount = formatCurrency(response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:Deductible'][0], opts);
						  

                          res.render('../ui/home.html', {
                              email: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:EmailID'][0],
                              name: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:Username'][0],
                              age: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:AgeRange'][0],
                              gender: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:Gender'][0],
							  premium: premiumAmount,
							  major: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:IsMajorMedicalEvent'][0],
							  deductible: deductibleAmount,
                              cancer: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:IsCancerTreatmentPlanned'][0],
                              smoking: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:IsSmoking'][0],
                              pregnancy: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:IsExpectingMother'][0],
                              country: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:Country'][0],
                              state: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:State'][0],
                              city: response['s:Envelope']['s:Body'][0].GetUserProfileResponse[0].GetUserProfileResult[0]['a:UserProfileDTO'][0]['a:City'][0]
                          });
                      }
                  });
              }             
            }
            console.log()
      });  
      
});
router.post('/registerUser', function (req, res) {
    userinfo.registerUser(req.body.username, req.body.email, req.body.password, function (err, response) {

        if (err)
        {
            console.log(err);
        }
        else
        {
            console.log("Registered Sucessfully " + JSON.stringify(response, null, 2));
            res.render('../ui/login.html', { info: "Registered successfully", display:"success", showMessage:"show"});
        }    

    });

});
module.exports = router;
