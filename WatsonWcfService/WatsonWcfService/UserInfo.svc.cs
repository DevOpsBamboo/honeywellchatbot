﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using WatsonWcfService.DataAccess;

namespace WatsonWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserInfo" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select UserInfo.svc or UserInfo.svc.cs at the Solution Explorer and start debugging.
    public class UserInfo : IUserInfo
    {
        public int Register(string UserName,string Email,string pwd)
        {
            RegistrationDetails registrationDetails = new RegistrationDetails();
            return registrationDetails.RegisterUser(UserName,Email,pwd);
        }

        public List<UserProfileDTO> GetUserProfile(string username)
        {
            RegistrationDetails registrationDetails = new RegistrationDetails();
            return registrationDetails.GetUserProfile(username);
        }

		public int UpdateProfile(string username, string premium, string deductible, string medicalEvents, string tobacco, string bloodPressure,
			string cardiacArrest, string pregnancy, string breastCancer)
		{
			RegistrationDetails registrationDetails = new RegistrationDetails();
			return registrationDetails.UpdateProfile(username, premium, deductible, medicalEvents, tobacco, bloodPressure,
			cardiacArrest, pregnancy, breastCancer);
		}

		public int UpdateAgeGender(string username, string ageRange, string gender)
        {
            RegistrationDetails registrationDetails = new RegistrationDetails();
            return registrationDetails.UpdateAgeGender(username, ageRange, gender);
        }

        public int UpdateLocInfo(string username,string country, string state, string city)
        {
            RegistrationDetails registrationDetails = new RegistrationDetails();
            return registrationDetails.UpdateLocInfo(username,country, state, city);
        }

        public int CheckUserLogin(string username, string password)
        {
            RegistrationDetails registrationDetails = new RegistrationDetails();
            return registrationDetails.CheckUserLogin(username, password);
        }
    }
}
