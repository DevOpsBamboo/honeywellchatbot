﻿USE [master]
GO
/****** Object:  Database [Watson]    Script Date: 7/3/2019 6:00:10 PM ******/
CREATE DATABASE [Watson]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Watson', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Watson.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Watson_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\Watson_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Watson] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Watson].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Watson] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Watson] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Watson] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Watson] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Watson] SET ARITHABORT OFF 
GO
ALTER DATABASE [Watson] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Watson] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [Watson] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Watson] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Watson] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Watson] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Watson] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Watson] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Watson] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Watson] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Watson] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Watson] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Watson] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Watson] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Watson] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Watson] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Watson] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Watson] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Watson] SET RECOVERY FULL 
GO
ALTER DATABASE [Watson] SET  MULTI_USER 
GO
ALTER DATABASE [Watson] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Watson] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Watson] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Watson] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'Watson', N'ON'
GO
USE [Watson]
GO
/****** Object:  User [WATSON]    Script Date: 7/3/2019 6:00:13 PM ******/
CREATE USER [WATSON] FOR LOGIN [WATSON] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBenfitPlans]    Script Date: 7/3/2019 6:00:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[usp_GetBenfitPlans]
(
@Deductable varchar(50) = null,
@Premium varchar(50) = null,
@Maternity varchar(50) = '-1',
@XRay varchar(50) = '-1',
@Medication varchar(50) = '-1',
@DoctorVisit varchar(50) = '-1',
@LabProcedure varchar(50) = '-1',
@Mammogram varchar(50) = '-1'
)
AS


select * from BenfitPlans where 
(@Deductable is null or CAST(Deductable AS int) <= CAST(@Deductable AS int))  
AND (@Premium is null or CAST(Premium AS int) <= CAST(@Premium AS int))  
AND (@XRay = '-1' or CAST(XRay AS int) <= CAST(@XRay AS int)) 
AND (@Mammogram = '-1' or CAST(Mammogram AS int) <= CAST(@Mammogram AS int)) 
AND (@DoctorVisit = '-1' or CAST(DoctorVisit AS int) <= CAST(@DoctorVisit AS int))
AND (@Maternity = '-1' or CAST(Maternity AS int) <= CAST(@Maternity AS int))
AND (@LabProcedure = '-1' or CAST(LabProcedure AS int) <= CAST(@LabProcedure AS int))
AND (@Medication = '-1' or CAST(Medication AS int) <= CAST(@Medication AS int))

GO
/****** Object:  StoredProcedure [dbo].[usp_GetBenfitPlans_Backup]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[usp_GetBenfitPlans_Backup]
(
@Gender varchar(50),
@PlanName nvarchar(100),
@PlanID nvarchar(100),
@FromAge int,
@ToAge int,
@City nvarchar(100),
@State nvarchar(100),
@Country nvarchar(100),
@NumofDependants int,
@DeductableAmount nvarchar(100),
@premiumAmount nvarchar(100),
@PlanAmount nvarchar(100),
@Term nvarchar(100),
@PlanType varchar(50),
@OOPM varchar(50),
@Coinsurance varchar(50),
@Deductable varchar(50),
@Premium varchar(50),
@Maternity varchar(50),
@Gynecology varchar(50),
@XRay varchar(50),
@Medication varchar(50),
@DoctorVisit varchar(50),
@LabProcedure varchar(50),
@Mammogram varchar(50),
@PrescriptionInsulin varchar(50)
)
AS

select * from BenfitPlans where (CAST(Deductable AS int) <= CAST(@Deductable AS int))  AND (CAST(Premium AS int) <= CAST(@Premium AS int))  AND (PrescriptionInsulin = @PrescriptionInsulin)


GO
/****** Object:  StoredProcedure [dbo].[usp_GetBenfitPlans1]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_GetBenfitPlans1]
(

@FromAge int,
@ToAge int

)
AS
select * from BenfitPlans where 
(
(@FromAge ='' or FromAge>=@FromAge)AND
(@ToAge ='' or Toage<=@ToAge))

GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateAgeGender]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateAgeGender]	
	@AgeRange varchar(20),
	@Gender varchar(20),
	@Username varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE UserProfile set AgeRange = @AgeRange, Gender = @Gender where Username=@Username
	
END


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateLocInfo]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateLocInfo]	
	@Country varchar(50),
	@State varchar(50),
	@City varchar(50),
	@Username varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	UPDATE UserProfile set [Country] = @Country,[State] = @State,[City]= @City where Username=@Username

END






GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateProfile]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateProfile]	
	@Name varchar(50),
	@Password Varchar(20),
	@Premium varchar(50),
	@Deductible varchar(50),
	@IsSmoking bit,
	@IsMajorMedicalEvent bit,
	@IsCancerTreatmentPlanned bit,
	@IsExpectingMother bit,
	@AgeRange varchar(20),
	@Gender varchar(20),
	@EmailID varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	--If(@UserID=0)	
	--INSERT INTO UserProfile([Name],[EmailID],[Password],[Premium],[Deductible],[IsSmoking],[IsMajorMedicalEvent],[IsCancerTreatmentPlanned],[IsExpectingMother],[AgeRange], [Gender])
	--VALUES(@Name,@EmailID,@Password,@Premium,@Deductible,@IsSmoking,@IsMajorMedicalEvent,@IsCancerTreatmentPlanned,@IsExpectingMother,@AgeRange,@Gender); 	
	--else if(@EmailID is Not Null or @UserID! =0)
	UPDATE UserProfile set Premium=@Premium, Deductible = @Deductible, IsSmoking = @IsSmoking, IsMajorMedicalEvent = @IsMajorMedicalEvent,IsCancerTreatmentPlanned = @IsMajorMedicalEvent, IsExpectingMother = @IsExpectingMother, AgeRange = @AgeRange, Gender = @Gender where EmailID=@EmailID
	
END


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateProfile_1]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateProfile_1]		
	@EmailID varchar(50)

AS
BEGIN

	SET NOCOUNT ON;

	--If(@UserID=0)	
	--INSERT INTO UserProfile([Name],[EmailID],[Password],[Premium],[Deductible],[IsSmoking],[IsMajorMedicalEvent],[IsCancerTreatmentPlanned],[IsExpectingMother],[AgeRange], [Gender])
	--VALUES(@Name,@EmailID,@Password,@Premium,@Deductible,@IsSmoking,@IsMajorMedicalEvent,@IsCancerTreatmentPlanned,@IsExpectingMother,@AgeRange,@Gender); 	
	--else if(@EmailID is Not Null or @UserID! =0)
	UPDATE UserProfile set EmailID='offero86@gmail.com' where EmailID=@EmailID
	
END


GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateProfile_2]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[usp_UpdateProfile_2]
	@Username varchar(50),
	@Premium varchar(50),
	@Deductible varchar(50),
	@IsSmoking varchar(50),
	@IsMajorMedicalEvent varchar(50),
	@IsCancerTreatmentPlanned varchar(50),
	@IsExpectingMother varchar(50),
	@CardiacArrest varchar(50),
	@BloodPressure varchar(50)
AS
BEGIN

	SET NOCOUNT ON;
	UPDATE UserProfile set Premium=@Premium, Deductible = @Deductible, IsSmoking = @IsSmoking, IsMajorMedicalEvent = @IsMajorMedicalEvent,
	IsCancerTreatmentPlanned = @IsCancerTreatmentPlanned, IsExpectingMother = @IsExpectingMother, BloodPressure = @BloodPressure, CardiacArrest = @CardiacArrest where Username=@Username
	
END




GO
/****** Object:  Table [dbo].[BenfitPlans]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BenfitPlans](
	[PlanID] [nvarchar](100) NOT NULL,
	[PlanName] [nvarchar](100) NOT NULL,
	[FromAge] [int] NULL,
	[ToAge] [int] NULL,
	[City] [nvarchar](100) NULL,
	[State] [nvarchar](100) NULL,
	[Country] [nvarchar](100) NULL,
	[NumberofDependants] [int] NULL,
	[PlanAmount] [nvarchar](100) NULL,
	[TermAmount] [nvarchar](100) NULL,
	[NoOfTerms] [nvarchar](100) NULL,
	[Gender] [varchar](50) NULL,
	[PostalCode] [varchar](50) NULL,
	[CountryCode] [varchar](50) NULL,
	[PlanType] [varchar](50) NULL,
	[OutOfPocketMaxFamily] [varchar](50) NULL,
	[OutOfPocketMaxIndividual] [varchar](50) NULL,
	[CoInsurance] [varchar](50) NULL,
	[CoPay] [varchar](50) NULL,
	[Deductable] [varchar](50) NULL,
	[Premium] [nvarchar](50) NULL,
	[Maternity] [varchar](50) NULL,
	[Gynecology] [varchar](50) NULL,
	[XRay] [varchar](50) NULL,
	[Medication] [varchar](50) NULL,
	[DoctorVisit] [varchar](50) NULL,
	[LabProcedure] [varchar](50) NULL,
	[Mammogram] [varchar](50) NULL,
	[PrescriptionInsulin] [varchar](50) NULL,
	[BenefitPeriod] [varchar](50) NULL,
	[PrimaryCare] [varchar](50) NULL,
	[SpecialistCare] [varchar](50) NULL,
	[DurableMedicalQuipment] [varchar](50) NULL,
	[Diagnostic] [varchar](50) NULL,
	[TherapyServcies] [varchar](50) NULL,
	[MentalAndSubstanceAbuse] [varchar](50) NULL,
	[DeductableFamily] [varchar](50) NULL,
	[PremiumAmount] [varchar](50) NULL,
	[AUTOID] [int] IDENTITY(1,1) NOT NULL,
	[IsDiabetesProgramAvailable] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[PlanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Claims]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Claims](
	[ClaimNumber] [int] NOT NULL,
	[BilledAmount] [decimal](8, 2) NULL,
	[AllowedAmount] [decimal](8, 2) NULL,
	[DiscountAmount] [decimal](8, 2) NULL,
	[ProcedureCode] [varchar](10) NULL,
	[ClaimRecvdDate] [date] NULL,
	[ServiceFromDate] [date] NULL,
	[ServiceToDate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[ClaimNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Member]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Member](
	[MemberID] [int] NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MemberID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MemberClaims]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemberClaims](
	[MemberClaimId] [int] NOT NULL,
	[MemberID] [int] NULL,
	[ClaimNumber] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MemberClaimId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MemberPlan]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MemberPlan](
	[MemberPlanID] [int] NOT NULL,
	[PlanID] [nvarchar](100) NULL,
	[NumberOfVisitsThisYear] [int] NULL,
	[MemberID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MemberPlanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PlanCoverdProcedures]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlanCoverdProcedures](
	[PlanID] [int] NOT NULL,
	[ProcedureCode] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[PlanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProcedureCode]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProcedureCode](
	[ProcedureCode] [varchar](10) NOT NULL,
	[ProcedureName] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProcedureCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ranges]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ranges](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RangeFrom] [int] NOT NULL,
	[RangeTo] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserProfile](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[EmailID] [varchar](50) NOT NULL,
	[Password] [varchar](20) NULL,
	[Premium] [varchar](50) NULL,
	[Deductible] [varchar](50) NULL,
	[IsSmoking] [varchar](20) NULL,
	[IsMajorMedicalEvent] [varchar](20) NULL,
	[IsCancerTreatmentPlanned] [varchar](20) NULL,
	[IsExpectingMother] [varchar](20) NULL,
	[AgeRange] [varchar](20) NULL,
	[Gender] [varchar](20) NULL,
	[BloodPressure] [varchar](20) NULL,
	[CardiacArrest] [varchar](20) NULL,
	[Country] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Username] [nvarchar](50) NOT NULL,
	[IsMember] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserProfile_old]    Script Date: 7/3/2019 6:00:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserProfile_old](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[EmailID] [varchar](50) NOT NULL,
	[Password] [varchar](20) NULL,
	[Premium] [varchar](50) NULL,
	[Deductible] [varchar](50) NULL,
	[IsSmoking] [varchar](20) NULL,
	[IsMajorMedicalEvent] [varchar](20) NULL,
	[IsCancerTreatmentPlanned] [varchar](20) NULL,
	[IsExpectingMother] [varchar](20) NULL,
	[AgeRange] [varchar](20) NULL,
	[Gender] [varchar](20) NULL,
	[BloodPressure] [varchar](20) NULL,
	[CardiacArrest] [varchar](20) NULL,
	[Country] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Username] [varchar](50) NULL,
	[IsMember] [bit] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[BenfitPlans] ON 

INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156928', N'Benefit-Choice-Platinum-PPO-Pro-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'300', N'8000', N'100', N'60', N'30', N'50', N'40', N'30', N'40', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 71, 1)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156929', N'Benefit-Choice-Platinum-PPO-Pro', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'350', N'4000', N'100', N'65', N'20', N'40', N'50', N'40', N'20', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 72, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156930', N'MaxBenefit-Choice-Platinum-PPO-Pro', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'350', N'4000', N'100', N'70', N'20', N'35', N'50', N'50', N'50', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 73, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156931', N'MaxBenefit-Choice-Platinum-PPO-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'350', N'6000', N'100', N'60', N'30', N'35', N'50', N'75', N'40', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 74, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156932', N'MaxBenefit-Care-Platinum-PPO-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'350', N'5000', N'100', N'60', N'30', N'35', N'50', N'60', N'30', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 75, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156933', N'Max-Care-Platinum-PPO-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'300', N'4000', N'100', N'80', N'30', N'35', N'40', N'20', N'50', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 78, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156934', N'Personal-Care-Platinum-PPO-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'450', N'6000', N'100', N'50', N'32', N'30', N'45', N'40', N'40', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 79, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156935', N'Personal-Care-Platinum-HDHP', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HDHP', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'450', N'6000', N'100', N'50', N'32', N'30', N'45', N'40', N'40', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 96, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156936', N'Personal-Care-Gold-HDHP', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HDHP', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'500', N'5000', N'75', N'50', N'32', N'30', N'45', N'40', N'40', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 97, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156937', N'Personal-Care-Silver-HDHP', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HDHP', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'600', N'4000', N'50', N'50', N'32', N'30', N'45', N'40', N'40', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 98, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'156938', N'Personal-Care-Bronze-HDHP', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HDHP', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'600', N'3000', N'15', N'50', N'32', N'30', N'45', N'40', N'40', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 99, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157345', N'Keystone-HMO-Silver-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'M', NULL, N'USA', N'HMO', N'$5,000 ', N'$2,500 ', N'yes', N'no', N'1000 ', N'10000', N'50', N'210', N'210', N'210', N'100', N'60', N'40', N'yes', N'Contract', N'$30 copay/visit', N'$40 copay/visit', N'50%', N'$40 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$250 copay/visit', N'$1,000 ', N'no', 21, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157346', N'Personal-Choice-PPO-Gold-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'M', NULL, N'USA', N'PPO', N'$5,000 ', N'$2,500 ', N'yes', N'no', N'2750', N'18000', N'75', N'270', N'220', N'250', N'300', N'350', N'35', N'yes', N'Contract', N'$30 copay/visit', N'$50 copay/visit', N'20%, after deductible', N'$50 copay/test(X-Ray)/No Charge (Blood Work)', N'$50 copay/visit', N'$250 copay/visit', N'$1,500 ', N'no', 22, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157404', N'Keystone-HMO-Choice-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'M', NULL, N'USA', N'HMO', N'$10,000 ', N'$5,000 ', N'yes', N'no', N'2500 ', N'6500', N'35', N'230', N'230', N'230', N'100', N'30', N'40', N'yes', N'Contract', N'$30 copay/visit', N'$50 copay/visit', N'30%, after deductible', N'$50 copay/test(X-Ray)/No Charge (Blood Work)', N'$50 copay/visit', N'$275 copay/visit', N'$2,500 ', N'no', 23, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157534', N'Personal-Choice-PPO-Bronze-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'M', NULL, N'USA', N'POS', N'$12,870 ', N'$6,435 ', N'no', N'yes', N'350', N'7500', N'15', N'240', N'240', N'240', N'100', N'50', N'45', N'no', N'Calendar', N'$30 copay/visit', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$300 copay/visit', N'$0 ', N'yes', 24, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157535', N'Personal-Choice-Bronze-Basic-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'M', NULL, N'USA', N'POS', N'$12,870 ', N'$6,435 ', N'no', N'yes', N'500', N'2500', N'15', N'250', N'250', N'250', N'100', N'20', N'60', N'no', N'Calendar', N'$30 copay/visit', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$350 copay/visit', N'$0 ', N'yes', 25, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157587', N'Keystone-HMO-Gold', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$3,000 ', N'$1,500 ', N'yes', N'no', N'800', N'4000', N'75', N'90', N'90', N'140', N'140', N'140', N'140', N'yes', N'Calendar', N'$15 copay/visit', N'$25 copay/visit', N'No Charge after deductible', N'No Charge', N'No Charge', N'$375 copay/visit', N'$0 ', N'no', 9, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157607', N'Keystone-HMO-Gold-Proactive-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'M', NULL, N'USA', N'POS', N'$800 ', N'$400 ', N'no', N'yes', N'100 ', N'25000', N'75', N'260', N'260', N'260', N'100', N'10', N'75', N'no', N'Calendar', N'$30 copay/visit', N'$5 copay/visit', N'No Charge', N'No Charge', N'$50 copay/visit', N'No Charge after deductible', N'$100 ', N'yes', 26, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157608', N'Personal-Choice-PPO-Bronze-Reserve-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'M', NULL, N'USA', N'POS', N'$6,000 ', N'$3,000 ', N'yes', N'no', N'2000 ', N'2000', N'15', N'270', N'270', N'270', N'100', N'20', N'100', N'yes', N'Calendar', N'$30 copay/visit', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'No Charge after deductible', N'$2,000 ', N'no', 27, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157610', N'Keystone-HMO-Gold-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, NULL, NULL, NULL, N'M', NULL, NULL, N'HMO', NULL, NULL, N'no', N'no', N'750', N'5000', N'75', N'70', N'50', N'60', N'70', N'80', N'50', N'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'no', 57, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157611', N'Keystone-HMO-Bronze-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, NULL, NULL, NULL, N'M', NULL, NULL, N'HMO', NULL, NULL, N'no', N'yes', N'350', N'6000', N'15', N'50', N'200', N'30', N'40', N'30', N'50', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 58, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157612', N'Max-Care-POS-Gold', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, NULL, NULL, NULL, N'M', NULL, NULL, N'POS', NULL, NULL, N'no', N'yes', N'400', N'10000', N'75', N'60', N'300', N'40', N'50', N'60', N'100', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 59, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157613', N'Max-Care-POS-Platinum', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, NULL, NULL, NULL, N'M', NULL, NULL, N'POS', NULL, NULL, N'yes', N'no', N'1000', N'12000', N'100', N'50', N'30', N'40', N'50', N'100', N'200', N'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'no', 60, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157614', N'Max-Care-POS-Silver', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, NULL, NULL, NULL, N'M', NULL, NULL, N'POS', NULL, NULL, N'no', N'yes', N'300', N'8000', N'50', N'80', N'50', N'60', N'70', N'80', N'40', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 61, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157615', N'Max-Care-POS-Bronze', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, NULL, NULL, NULL, N'M', NULL, NULL, N'POS', NULL, NULL, N'yes', N'no', N'900', N'10000', N'15', N'70', N'100', N'40', N'20', N'100', N'100', N'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'no', 62, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157616', N'Max-Choice-Bronze-Basic', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'yes', N'no', N'800', N'10000', N'15', N'20', N'60', N'60', N'40', N'40', N'40', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 63, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157618', N'Keystone-Choice-Bronze-Basic', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'yes', N'no', N'800', N'10000', N'15', N'30', N'10', N'60', N'30', N'30', N'20', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 64, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157619', N'Keystone-Choice-Bronze-Basic-Proactive', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'no', N'no', N'750', N'8000', N'15', N'80', N'10', N'60', N'40', N'50', N'60', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 65, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157620', N'Keystone-Choice-Bronze-Basic-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'700', N'9000', N'15', N'20', N'70', N'60', N'30', N'40', N'50', N'no', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'yes', 66, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157621', N'Keystone-HMO-Gold-Proactive', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$12,000 ', N'$6,000 ', N'yes', N'no', N'2500 ', N'20000', N'75', N'100', N'100', N'60', N'60', N'60', N'60', N'yes', N'Calendar', N'$30 copay/visit', N'$50 copay/visit', N'20%, after deductible', N'$50 copay/test(X-Ray)/No Charge (Blood Work)', N'$50 copay/test(X-Ray)/No Charge (Blood Work)', N'$275 copay/visit', N'$2,500 ', N'no', 10, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157622', N'Max-PPO-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'200', N'5000', N'35', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 87, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157623', N'Max-PPO-Basic', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'200', N'5000', N'35', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 88, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157624', N'Max-PPO-Bronze', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'200', N'5000', N'15', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 89, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157625', N'Max-HMO-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'300', N'4000', N'35', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 90, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157626', N'Max-HMO-Basic', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'250', N'3000', N'35', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 91, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157627', N'Max-HMO-Bronze', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'200', N'2000', N'15', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 92, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157628', N'Max-POS-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'POS', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'200', N'5000', N'35', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 93, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157629', N'Max-POS-Basic', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'POS', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'200', N'5000', N'35', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 94, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157630', N'Max-POS-Bronze', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'POS', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'200', N'5000', N'15', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 95, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157646', N'Keystone-HMO-Silver', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$20,000 ', N'$10,000 ', N'no', N'yes', N'200', N'2000', N'50', N'110', N'110', N'70', N'70', N'70', N'70', N'no', N'Calendar', N'$20 copay/visit', N'$40 copay/visit', N'75%', N'75%', N'75%', N'$275 copay/visit', N'$0 ', N'yes', 11, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157649', N'Keystone-HMO-Silver-Proactive', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$11,200 ', N'$5,600 ', N'yes', N'no', N'1750 ', N'18000', N'50', N'120', N'120', N'80', N'80', N'80', N'80', N'yes', N'Calendar', N'$20 copay/visit', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'$1,750 ', N'no', 12, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157657', N'Personal-Choice-PPO-Platinum', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'M', NULL, N'USA', N'PPO', N'$3,000 ', N'$1,500 ', N'yes', N'no', N'800', N'9000', N'100', N'100', N'100', N'100', N'100', N'100', N'100', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 1, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157658', N'Personal-Choice-PPO-Gold', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$20,000 ', N'$1,500 ', N'no', N'yes', N'200', N'5000', N'75', N'20', N'20', N'20', N'20', N'20', N'20', N'no', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'yes', 2, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157659', N'Personal-Choice-PPO-Silver', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$11,200 ', N'$6,000 ', N'no', N'no', N'750', N'4000', N'50', N'30', N'30', N'30', N'30', N'30', N'30', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'Covered 30%, after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 3, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157664', N'Keystone-HMO-Silver-Proactive-Value', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$11,200 ', N'$5,600 ', N'yes', N'no', N'1700', N'9000', N'50', N'130', N'130', N'90', N'90', N'90', N'90', N'yes', N'Calendar', N'$20 copay/visit', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'$1,500 ', N'no', 13, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157671', N'Personal-Choice-PPO-Bronze', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$11,200 ', N'$10,000 ', N'no', N'yes', N'300', N'8000', N'15', N'40', N'40', N'40', N'40', N'40', N'40', N'no', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'Covered 30%, after deductible', N'$35 copay/test', N'$35 copay/visit', N'Not Covered', N'$0', N'yes', 4, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157672', N'Personal-Choice-PPO-Bronze-Reserve', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$12,700 ', N'$5,600 ', N'no', N'yes', N'550', N'2000', N'15', N'50', N'50', N'50', N'60', N'60', N'60', N'no', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'yes', 5, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157673', N'Personal-Choice-Bronze-Basic', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'yes', N'no', N'800', N'7000', N'15', N'60', N'60', N'60', N'80', N'80', N'80', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 6, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157674', N'Personal-Choice-Catastrophic', 18, 60, N'San Francisco', N'CA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$2,500 ', N'no', N'yes', N'400', N'5000', N'35', N'70', N'70', N'70', N'100', N'100', N'100', N'no', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'yes', 7, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157686', N'Max-Benefit-PPO-Platinum-Plus', 18, 60, N'San Francisco', N'CA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$12,700 ', N'$6,350 ', N'yes', N'no', N'3000 ', N'25000', N'100', N'280', N'320', N'350', N'350', N'350', N'350', N'yes', N'Calendar', N'$20 copay/visit', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'No Charge after deductible', N'$3,000 ', N'no', 18, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157704', N'Keystone-HMO-Platinum', 18, 60, N'San Francisco', N'CA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$5,000 ', N'$2,500 ', N'no', N'yes', N'600', N'2000', N'100', N'80', N'80', N'150', N'150', N'150', N'150', N'no', N'Contract', N'$25 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'yes', 8, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157716', N'Keystone-HMO-Bronze', 18, 60, N'San Francisco', N'CA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$5,000 ', N'$2,500 ', N'no', N'yes', N'300', N'14000', N'15', N'140', N'140', N'100', N'100', N'100', N'100', N'no', N'Calendar', N'$20 copay/visit', N'$20 copay/visit', N'No Charge after deductible', N'No Charge', N'No Charge', N'No Charge after deductible', N'$0 ', N'yes', 14, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157754', N'Max-Benefit-POS-Platinum', 18, 60, N'San Francisco', N'CA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'POS', N'$5,000 ', N'$2,500 ', N'no', N'yes', N'400', N'4000', N'100', N'150', N'90', N'100', N'50', N'50', N'50', N'no', N'Calendar', N'$20 copay/visit', N'$40 copay/visit', N'50%', N'No Charge', N'No Charge', N'$250 copay/visit', N'$0 ', N'yes', 15, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157957', N'Max-Benefit-PPO-Gold', 18, 60, N'San Francisco', N'CA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$0 ', N'$4,500 ', N'yes', N'no', N'2250', N'16000', N'75', N'300', N'300', N'300', N'300', N'300', N'300', N'yes', N'Calendar', N'$20 copay/visit', N'$30 copay/visit', N'20%, after deductible', N'$25 copay/visit', N'$25 copay/visit', N'$200 copay/visit', N'$750 ', N'no', 16, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157958', N'Personal-Choice-Platinum-PPO-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'yes', N'no', N'2500', N'20000', N'100', N'290', N'310', N'320', N'320', N'320', N'320', N'yes', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'no', 17, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157959', N'Personal-Choice-Platinum-PPO-Proactive', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'600', N'8000', N'100', N'70', N'70', N'80', N'40', N'70', N'75', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 67, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157960', N'Personal-Choice-Platinum-PPO-Extra', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'550', N'8000', N'100', N'70', N'70', N'80', N'30', N'90', N'90', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 68, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157961', N'Max-Benefit-POS-Platinum-Extra', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'POS', N'$3,000 ', N'$1,500 ', N'no', N'yes', N'500 ', N'5000', N'100', N'190', N'190', N'120', N'100', N'30', N'80', N'no', N'Contract', N'0%', N'$40 copay/visit', N'No Charge after deductible', N'$40 copay/test, (X-Ray)/No Charge (Blood Work)', N'$50 copay/visit', N'No Charge after deductible', N'$500 ', N'yes', 19, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157962', N'Personal-Choice-Platinum-PPO-Pro-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'500', N'7000', N'100', N'75', N'40', N'60', N'60', N'50', N'40', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 69, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157963', N'Keystone-HMO-Platinum-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'HMO', N'$5,000 ', N'$2,500 ', N'yes', N'no', N'1500 ', N'7500', N'100', N'200', N'200', N'200', N'100', N'40', N'50', N'yes', N'Contract', N'$30 copay/visit', N'$40 copay/visit', N'20%, after deductible', N'$40 copay/test, (X-Ray)/No Charge (Blood Work)', N'$50 copay/visit', N'$200 copay/visit', N'$1,500 ', N'no', 20, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157964', N'Max-Choice-Platinum-PPO-Pro-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$14,300 ', N'$7,150 ', N'no', N'yes', N'400', N'5000', N'100', N'80', N'50', N'50', N'50', N'40', N'30', N'no', N'Contract', N'0%', N'$50 copay/visit', N'50%', N'$50 copay/test(X-Ray)/No Charge(Blood Work)', N'$50 copay/visit', N'$400 copay/visit', N'$0 ', N'yes', 70, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157965', N'Max-Choice-Bronze-Basic', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'yes', N'no', N'800', N'10000', N'15', N'20', N'60', N'60', N'40', N'40', N'40', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 81, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157966', N'Keystone-Choice-Bronze-Basic', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'yes', N'no', N'800', N'10000', N'15', N'30', N'10', N'60', N'30', N'30', N'20', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 82, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157967', N'Max-Choice-Bronze-Basic-Plus', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'yes', N'no', N'800', N'10000', N'15', N'20', N'5', N'60', N'40', N'40', N'40', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 83, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157968', N'Max-Choice-Bronze-Basic-Plus-Pro', 18, 60, N'Philadelphia', N'PA', N'USA', 5, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'yes', N'no', N'800', N'10000', N'15', N'20', N'10', N'60', N'40', N'40', N'40', N'yes', N'Contract', N'$20 copay/visit', N'$35 copay/visit', N'No Charge after deductible', N'No Charge', N'$35 copay/visit', N'Not Covered', N'$0', N'no', 84, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157969', N'Keystone-PPO-Gold-Plus-Pro', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'350', N'5000', N'75', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 85, NULL)
INSERT [dbo].[BenfitPlans] ([PlanID], [PlanName], [FromAge], [ToAge], [City], [State], [Country], [NumberofDependants], [PlanAmount], [TermAmount], [NoOfTerms], [Gender], [PostalCode], [CountryCode], [PlanType], [OutOfPocketMaxFamily], [OutOfPocketMaxIndividual], [CoInsurance], [CoPay], [Deductable], [Premium], [Maternity], [Gynecology], [XRay], [Medication], [DoctorVisit], [LabProcedure], [Mammogram], [PrescriptionInsulin], [BenefitPeriod], [PrimaryCare], [SpecialistCare], [DurableMedicalQuipment], [Diagnostic], [TherapyServcies], [MentalAndSubstanceAbuse], [DeductableFamily], [PremiumAmount], [AUTOID], [IsDiabetesProgramAvailable]) VALUES (N'157970', N'Max-PPO-Plus-Pro', 18, 60, N'Philadelphia', N'PA', N'USA', NULL, N'300000', NULL, N'100', N'F', NULL, N'USA', N'PPO', N'$5,000 ', N'$6,350 ', N'no', N'yes', N'300', N'5000', N'35', N'10', N'10', N'10', N'10', N'10', N'10', N'no', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'yes', 86, NULL)
SET IDENTITY_INSERT [dbo].[BenfitPlans] OFF
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (1, CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), CAST(100.00 AS Decimal(8, 2)), N'0001F', CAST(0xB63D0B00 AS Date), CAST(0xDE3D0B00 AS Date), CAST(0xE83D0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (2, CAST(5000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0001M', CAST(0xF13D0B00 AS Date), CAST(0xBF3D0B00 AS Date), CAST(0xC93D0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (3, CAST(8000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0001U', CAST(0xFA3D0B00 AS Date), CAST(0xC93D0B00 AS Date), CAST(0xCE3D0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (4, CAST(6000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0002M', CAST(0x563E0B00 AS Date), CAST(0xCA3D0B00 AS Date), CAST(0xD13D0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (5, CAST(6000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0002U', CAST(0x5B3E0B00 AS Date), CAST(0x053E0B00 AS Date), CAST(0x0C3E0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (6, CAST(5000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0002U', CAST(0x8A3E0B00 AS Date), CAST(0xD53D0B00 AS Date), CAST(0x423F0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (7, CAST(5000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0002U', CAST(0x8A3E0B00 AS Date), CAST(0xD53D0B00 AS Date), CAST(0x423F0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (8, CAST(5000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0002U', CAST(0x8A3E0B00 AS Date), CAST(0xD53D0B00 AS Date), CAST(0x423F0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (9, CAST(5000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0002U', CAST(0x8A3E0B00 AS Date), CAST(0xD53D0B00 AS Date), CAST(0xF73F0B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (10, CAST(10000.00 AS Decimal(8, 2)), CAST(2000.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0002U', CAST(0x8A3E0B00 AS Date), CAST(0xD53D0B00 AS Date), CAST(0x34400B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (11, CAST(9000.00 AS Decimal(8, 2)), CAST(1500.00 AS Decimal(8, 2)), CAST(500.00 AS Decimal(8, 2)), N'0002U', CAST(0xD53D0B00 AS Date), CAST(0xB63D0B00 AS Date), CAST(0x34400B00 AS Date))
INSERT [dbo].[Claims] ([ClaimNumber], [BilledAmount], [AllowedAmount], [DiscountAmount], [ProcedureCode], [ClaimRecvdDate], [ServiceFromDate], [ServiceToDate]) VALUES (12, CAST(10000.00 AS Decimal(8, 2)), CAST(2000.00 AS Decimal(8, 2)), CAST(1000.00 AS Decimal(8, 2)), N'0002U', CAST(0xF13D0B00 AS Date), CAST(0xD53D0B00 AS Date), CAST(0x71400B00 AS Date))
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (1, N'nltanniru')
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (2, N'ntanniru')
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (3, N'ntanniru1')
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (4, N'sparsa')
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (10, N'ntanniru10')
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (11, N'ferrouser1')
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (12, N'ferrouser2')
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (13, N'ferrouser3')
INSERT [dbo].[Member] ([MemberID], [UserName]) VALUES (14, N'ferrouser4')
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (1, 1, 1)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (2, 1, 2)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (3, 4, 3)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (4, 4, 4)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (5, 4, 5)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (6, 11, 6)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (7, 11, 7)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (8, 12, 8)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (9, 12, 8)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (10, 13, 9)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (11, 13, 9)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (12, 14, 10)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (13, 14, 11)
INSERT [dbo].[MemberClaims] ([MemberClaimId], [MemberID], [ClaimNumber]) VALUES (14, 14, 12)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (1, N'156928', 5, 10)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (2, N'156929', 15, 1)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (3, N'156930', 10, 4)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (4, N'156931', 25, 3)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (5, N'156932', 20, 2)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (6, N'156933', 10, 11)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (7, N'156934', 5, 12)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (8, N'156935', 11, 13)
INSERT [dbo].[MemberPlan] ([MemberPlanID], [PlanID], [NumberOfVisitsThisYear], [MemberID]) VALUES (9, N'156936', 15, 14)
INSERT [dbo].[PlanCoverdProcedures] ([PlanID], [ProcedureCode]) VALUES (156928, N'0001U')
INSERT [dbo].[PlanCoverdProcedures] ([PlanID], [ProcedureCode]) VALUES (156930, N'0002U')
INSERT [dbo].[PlanCoverdProcedures] ([PlanID], [ProcedureCode]) VALUES (156933, N'0001M')
INSERT [dbo].[PlanCoverdProcedures] ([PlanID], [ProcedureCode]) VALUES (156934, N'0001M')
INSERT [dbo].[PlanCoverdProcedures] ([PlanID], [ProcedureCode]) VALUES (156935, N'0002U')
INSERT [dbo].[PlanCoverdProcedures] ([PlanID], [ProcedureCode]) VALUES (156936, N'0002U')
INSERT [dbo].[ProcedureCode] ([ProcedureCode], [ProcedureName]) VALUES (N'0001F', N'Composite Measures')
INSERT [dbo].[ProcedureCode] ([ProcedureCode], [ProcedureName]) VALUES (N'0001M', N'Administrative Codes for Multianalyte Assays with Algorithmic Analyses (MAAA)')
INSERT [dbo].[ProcedureCode] ([ProcedureCode], [ProcedureName]) VALUES (N'0001U', N'Proprietary Laboratory Analyses (PLA) Codes')
INSERT [dbo].[ProcedureCode] ([ProcedureCode], [ProcedureName]) VALUES (N'0002M', N'Administrative Codes for Multianalyte Assays with Algorithmic Analyses (MAAA)')
INSERT [dbo].[ProcedureCode] ([ProcedureCode], [ProcedureName]) VALUES (N'0002U', N'Proprietary Laboratory Analyses (PLA) Codes')
SET IDENTITY_INSERT [dbo].[Ranges] ON 

INSERT [dbo].[Ranges] ([Id], [RangeFrom], [RangeTo]) VALUES (1, 1, 4)
INSERT [dbo].[Ranges] ([Id], [RangeFrom], [RangeTo]) VALUES (2, 5, 9)
INSERT [dbo].[Ranges] ([Id], [RangeFrom], [RangeTo]) VALUES (3, 10, 14)
INSERT [dbo].[Ranges] ([Id], [RangeFrom], [RangeTo]) VALUES (4, 20, 24)
SET IDENTITY_INSERT [dbo].[Ranges] OFF
SET IDENTITY_INSERT [dbo].[UserProfile] ON 

INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (12, N'Ferro User 1', N'ferrouser1@ferro.com', N'test', N'9500.50', N'5000', N'0', N'0', N'0', N'0', N'25-45', N'male', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'ferrouser1', 1)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (13, N'Ferro User 2', N'ferrouser2@ferro.com', N'test', N'9500.50', N'5000', N'0', N'0', N'0', N'0', N'25-45', N'male', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'ferrouser2', 1)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (14, N'Ferro User 3', N'ferrouser3@ferro.com', N'test', N'9500.50', N'5000', N'0', N'0', N'0', N'0', N'25-45', N'male', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'ferrouser3', 1)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (15, N'Ferro User 4', N'ferrouser4@ferro.com', N'test', N'9500.50', N'5000', N'0', N'0', N'0', N'0', N'25-45', N'male', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'ferrouser4', 1)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (1008, NULL, N'new@cg.com', N'new', N'3000', N'300', N'no', N'yes', N'no', N'no', NULL, NULL, N'no', N'no', N'USA', N'Philadelphia', N'PA', N'new', NULL)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (4, N'NagaLakshmi', N'nltanniru@liquidhub.com', N'test', N'9500.50', N'450', N'0', N'0', N'0', N'0', N'25-50', N'female', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'nltanniru', 1)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (3, N'NagaLakshmi', N'ntanniru@liquidhub.com', N'test', N'9500.50', N'5000', N'0', N'0', N'0', N'0', N'35-50', N'female', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'ntanniru', 0)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (2, N'NagaLakshmi', N'ntanniru1@liquidhub.com', N'test', N'9500.50', N'500', N'0', N'0', N'0', N'0', N'40-50', N'female', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'ntanniru1', 1)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (1, N'NagaLakshmi', N'ntanniru10@liquidhub.com', N'test', N'9500.50', N'50', N'0', N'0', N'0', N'0', N'40-45', N'female', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'ntanniru10', 1)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (5, N'Suneeta', N'sparsa@liquidhub.com', N'test', N'9500.50', N'450', N'0', N'0', N'0', N'0', N'25-45', N'female', N'0', N'0', N'USA', N'Philadelphia', N'PA', N'sparsa', 1)
INSERT [dbo].[UserProfile] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (1003, NULL, N'test', N'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', N'test', NULL)
SET IDENTITY_INSERT [dbo].[UserProfile] OFF
SET IDENTITY_INSERT [dbo].[UserProfile_old] ON 

INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (8, N'Syam', N'syam@gmail.com', N'test', N'0', N'900', N'Yes', N'Yes', N'Yes', N'Yes', N'35-44', N'MALE', N'Yes', N'Yes', N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (9, N'Chris', N'chris@gmail.com', N'test', N'9500.50', N'30', N'0', N'0', N'0', N'0', N'35-38', N'Male', N'0', N'0', N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (10, N'suneeta', N'suneeta@gmail.com', N'test', N'9500.50', N'40', N'0', N'0', N'0', N'0', N'35-40', N'female', N'0', N'0', N'USA', N'Philadelphia', N'PA', NULL, 1)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (48, N'rashmeet', N'rashmeet@gmail.com', N'test@123', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0', N'0', N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (49, N'8aug20171@gmail.com', N'8aug20171@gmail.com', N'8aug20171@gmail.com', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0', N'0', N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (50, N'sun@lh.com', N'sun@lh.com', N'sun@lh.com', N'10000', N'900', N'no', N'yes', N'yes', N'no', N'18-24', N'FEMALE', N'yes', N'no', N'USA', N'Philadelphia', N'PA', N'sun', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (51, N'9aug@gmail.com', N'9aug@gmail.com', N'9aug@gmail.com', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'0', N'0', N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (52, N'16aug2017@gmail.com', N'16aug2017@gmail.com', N'16aug2017@gmail.com', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (53, N'testtest', N'16aug20171@gmail.com', N'16aug20171@gmail.com', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (54, N'rajul rana', N'rrana@Liquidhub.com', N'test1234', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (55, N'undefined', N'undefined', N'undefined', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (56, N'undefined', N'undefined', N'undefined', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (57, N'undefined', N'undefined', N'undefined', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (58, N'undefined', N'undefined', N'undefined', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (59, N'undefined', N'undefined', N'undefined', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (60, N'undefined', N'undefined', N'undefined', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (61, N'undefined', N'undefined', N'undefined', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (65, N'usertest@lh.com', N'usertest@lh.com', N'usertest', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (68, N'cnico', N'cnicodemus@liquidhub.com', N'1234', N'50000', N'0', N'no', N'no', N'no', N'no', NULL, NULL, N'no', N'no', N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (71, NULL, N'sjose@liquidhub.com', N'user123', N'2000', N'300', N'yes', N'no', N'yes', N'no', NULL, NULL, N'yes', N'no', N'USA', N'Philadelphia', N'PA', N'sjose', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (72, NULL, N'rgarlapati@liquidhub.com', N'Apollo#1', N'1000', N'15000', N'no', N'yes', N'no', N'yes', NULL, NULL, N'no', N'no', N'USA', N'Philadelphia', N'PA', N'raghu', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (78, NULL, N'sparsa@liquidhub.com', N'qwer1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', N'sprasa', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (82, NULL, N'sparsa@liquidhub.com', N'dddd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', N'suneeta', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (83, NULL, N'vtank@liquidhub.com', N'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', N'Vinesh', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (62, N'22aug2017@gmail.com', N'22aug2017@gmail.com', N'22aug2017@gmail.com', N'9500.50', NULL, NULL, NULL, NULL, NULL, N'35-44', N'MALE', NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (84, NULL, N'prashant@google.com', N'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', N'prashant', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (63, N'testuser', N'testuser@lh.com', N'testuser', N'9500.50', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (64, N'suneeta parsa', N'sparsa@liquidhub.com', N'asdfghjk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (69, NULL, N'cnicodemus@liquidhub.com', N'1234', N'10000', N'900', N'yes', N'no', N'yes', N'no', N'35-44', N'MALE', N'no', N'yes', N'USA', N'Philadelphia', N'PA', N'nico', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (73, NULL, N'sparsa@liquidhub.com', N'qwer1234', N'10000', N'2', N'yes', N'yes', N'no', N'no', NULL, NULL, N'no', N'no', N'USA', N'Philadelphia', N'PA', N'sparsa', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (74, NULL, N'cnicodemus@liquidhub.com', N'test', N'1000', N'200', N'yes', N'Yes', N'No', N'Yes', NULL, NULL, N'yes', N'yes', N'USA', N'Philadelphia', N'PA', N'test', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (75, NULL, N'mford@liquidhub.com', N'M262814471', N'4000', N'500', N'no', N'no', N'no', N'no', NULL, NULL, N'no', N'no', N'USA', N'Philadelphia', N'PA', N'mford', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (76, NULL, N'syam@gmail.com', N'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', N'syam', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (77, NULL, N'syam@gmail.com', N'syam@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', N'syam@gmail.com', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (80, NULL, N'shill@liquidhub.com', N'shill', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', N'shill', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (81, NULL, N'16oct2017@gmail.com', N'16oct2017', N'3000', N'200', N'Yes', N'no', N'yes', N'No', NULL, NULL, N'Yes', N'Yes', N'USA', N'Philadelphia', N'PA', N'16oct2017', NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (66, N'sparsa1', N'sparsa1@liquidhub.com', N'SPARSA1234', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (67, N'sparsa1', N'sparsa1@liquidhub.com', N'sparsa12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'USA', N'Philadelphia', N'PA', NULL, NULL)
INSERT [dbo].[UserProfile_old] ([UserID], [Name], [EmailID], [Password], [Premium], [Deductible], [IsSmoking], [IsMajorMedicalEvent], [IsCancerTreatmentPlanned], [IsExpectingMother], [AgeRange], [Gender], [BloodPressure], [CardiacArrest], [Country], [City], [State], [Username], [IsMember]) VALUES (79, NULL, N'shill@liquidhub.com', N'bob9669', N'500', N'500', N'no', N'no', N'no', N'no', NULL, NULL, N'no', N'no', N'USA', N'Philadelphia', N'PA', N'inamarina', NULL)
SET IDENTITY_INSERT [dbo].[UserProfile_old] OFF
ALTER TABLE [dbo].[UserProfile] ADD  DEFAULT ('USA') FOR [Country]
GO
ALTER TABLE [dbo].[UserProfile] ADD  DEFAULT ('Philadelphia') FOR [City]
GO
ALTER TABLE [dbo].[UserProfile] ADD  DEFAULT ('PA') FOR [State]
GO
ALTER TABLE [dbo].[UserProfile_old] ADD  DEFAULT ('USA') FOR [Country]
GO
ALTER TABLE [dbo].[UserProfile_old] ADD  DEFAULT ('Philadelphia') FOR [City]
GO
ALTER TABLE [dbo].[UserProfile_old] ADD  DEFAULT ('PA') FOR [State]
GO
ALTER TABLE [dbo].[Claims]  WITH CHECK ADD FOREIGN KEY([ProcedureCode])
REFERENCES [dbo].[ProcedureCode] ([ProcedureCode])
GO
ALTER TABLE [dbo].[Member]  WITH CHECK ADD FOREIGN KEY([UserName])
REFERENCES [dbo].[UserProfile] ([Username])
GO
ALTER TABLE [dbo].[MemberClaims]  WITH CHECK ADD FOREIGN KEY([ClaimNumber])
REFERENCES [dbo].[Claims] ([ClaimNumber])
GO
ALTER TABLE [dbo].[MemberClaims]  WITH CHECK ADD FOREIGN KEY([MemberID])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [dbo].[MemberPlan]  WITH CHECK ADD FOREIGN KEY([MemberID])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [dbo].[MemberPlan]  WITH CHECK ADD FOREIGN KEY([PlanID])
REFERENCES [dbo].[BenfitPlans] ([PlanID])
GO
ALTER TABLE [dbo].[PlanCoverdProcedures]  WITH CHECK ADD FOREIGN KEY([ProcedureCode])
REFERENCES [dbo].[ProcedureCode] ([ProcedureCode])
GO
USE [master]
GO
ALTER DATABASE [Watson] SET  READ_WRITE 
GO
