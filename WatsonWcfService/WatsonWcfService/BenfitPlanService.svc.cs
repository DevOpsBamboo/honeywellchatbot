﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WatsonWcfService.DataAccess;

namespace WatsonWcfService
{
    
    public class BenfitPlanService : IBenfitPlanService
    {

        public List<BenefitPlanDto> GetBenfitPlans( string deductableAmount,string gender, string fromAge, string ToAge,
                                                   string premium, string maternity, string gynecology,
                                                   string xRay, string medication, string doctorVisit,
                                                   string labProcedure, string mammogram, string prescriptionInsulin)
        {
            try
            {
                DataAccess.BenefitPlanRequestDto request = new BenefitPlanRequestDto();               
                request.Gender = gender;
                request.Deductable = deductableAmount;
                request.FromAge = Convert.ToInt32( fromAge);
                request.ToAge = Convert.ToInt32( ToAge);
                request.Premium = premium;
                request.Maternity = maternity;
                request.Gynecology = gynecology;
                request.XRay = xRay;
                request.Medication = medication;
                request.DoctorVisit = doctorVisit;
                request.LabProcedure = labProcedure;
                request.Mammogram = mammogram;
                request.PrescriptionInsulin = prescriptionInsulin;
                BenefitPlan objPlan = new BenefitPlan();
                return objPlan.GetBenfitPlans(request);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

		public List<BenefitPlanDto> GetAllBenefitPlans()
		{
			try
			{	
				BenefitPlan objPlan = new BenefitPlan();
				return objPlan.GetAllBenefitPlans();

			}
			catch (Exception ex)
			{

				throw ex;
			}
		}
        
        public List<BenefitPlanDto> GetBenefitPlansbyLoc(string Country, string State, string City)
        {
            try
            {
                BenefitPlan objPlan = new BenefitPlan();
                List<BenefitPlanDto> result = null;
                if(Country != null && State!=null && City != null)
                {
                   result = objPlan.GetAllBenefitPlans().Where(item => item.Country == Country && item.State == State && item.City == City).ToList();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
