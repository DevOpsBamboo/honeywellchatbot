﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web;
using WatsonWcfService.DataAccess;

namespace WatsonWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserInfo" in both code and config file together.
    [ServiceContract]
    public interface IUserInfo
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                       BodyStyle = WebMessageBodyStyle.Wrapped,
                                        RequestFormat = WebMessageFormat.Json,
                                        UriTemplate = "/RegistrationDetailsDTO")]

        int Register(string UserName,string Email,string pwd);

        [OperationContract]
        List<UserProfileDTO> GetUserProfile(string username);
        
		[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "UpdateProfile/{UserID}")]
		[OperationContract]
		int UpdateProfile(string username, string premium, string deductible, string medicalEvents, string tobacco, string bloodPressure,
			string cardiacArrest, string pregnancy, string breastCancer);

		[OperationContract]
        int UpdateAgeGender(string username, string ageRange, string gender);

        [OperationContract]
        int UpdateLocInfo(string username,string country, string state, string city);
        
        [OperationContract]
        int CheckUserLogin(string username, string password);
    }
}
