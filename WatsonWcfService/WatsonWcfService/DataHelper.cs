﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WatsonWcfService
{
    public class DataHelper
    {
        public const string ConnectionStringName = "WatsonService";
        public string ConnectionString = ConfigurationManager.ConnectionStrings[ConnectionStringName].ToString();
        public DataSet GetDataSet(string procedure, params SqlParameter[] sqlParameters)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = procedure;

                    foreach (SqlParameter sqlParameter in sqlParameters)
                    {
                        cmd.Parameters.Add(sqlParameter);
                    }
                    DataSet ds = new DataSet();
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    sda.Fill(ds);

                    return ds;
                }
            }
            catch (Exception ex)
            {               
                throw ex;
            }
        }

		public DataSet GetAllDataSet(string SQL)
		{
			SqlConnection conn = new SqlConnection(ConnectionString);
			SqlDataAdapter da = new SqlDataAdapter();
			SqlCommand cmd = conn.CreateCommand();
			cmd.CommandText = SQL;
			da.SelectCommand = cmd;
			DataSet ds = new DataSet();

			conn.Open();
			da.Fill(ds);
			conn.Close();

			return ds;
		}


        public int InsertRecord(string SQL )
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
          //  SqlDataAdapter da = new SqlDataAdapter();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = SQL;
            conn.Open();
            int inserted = Convert.ToInt32(cmd.ExecuteScalar());
            conn.Close();
            return inserted;
           // da.InsertCommand = cmd;

            //try
            //{

            //    da.InsertCommand.ExecuteNonQuery();
            //    return true;
            //}
            //catch (Exception ex)
            //{
            //    return false;
            //}

        }
        public int UpdateRecord(string procedure, params SqlParameter[] sqlParameters)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = procedure;

                    foreach (SqlParameter sqlParameter in sqlParameters)
                    {
                        cmd.Parameters.Add(sqlParameter);
                    }
               
                    return  cmd.ExecuteNonQuery();
                
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
}