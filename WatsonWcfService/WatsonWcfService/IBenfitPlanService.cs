﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WatsonWcfService.DataAccess;

namespace WatsonWcfService
{
    
    [ServiceContract]
    public interface IBenfitPlanService
    {
        //[OperationContract]
        //List<BenefitPlanDto> GetBenfitPlans(BenefitPlanRequestDto request);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat =WebMessageFormat.Json,
                                    BodyStyle =WebMessageBodyStyle.Wrapped,
                                     RequestFormat =WebMessageFormat.Xml,
                                     UriTemplate = "GetBenfitPlans/{copay}/{deductableAmount}/{gender}/{fromAge}/{ToAge}")]
        List<BenefitPlanDto> GetBenfitPlans(string deductableAmount,string gender, string fromAge,string ToAge,
                                            string premium,string maternity,string gynecology,
                                            string xRay,string medication,string doctorVisit,
                                            string labProcedure,string mammogram,string prescriptionInsulin);

		[OperationContract]
		List<BenefitPlanDto> GetAllBenefitPlans();

        [OperationContract]
        List<BenefitPlanDto> GetBenefitPlansbyLoc(string Country,string State,string City);
    }

}
