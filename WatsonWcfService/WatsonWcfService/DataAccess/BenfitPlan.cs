﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;
namespace WatsonWcfService.DataAccess
{
    [DataContract]    
    public class BenefitPlanDto
    {
        [DataMember]
        public string PlanID { get; set; }
        [DataMember]
        public string PlanName { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string City { get; set; }        
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public int NumberofDependants { get; set; }
        [DataMember]
        public string PremiumAmount { get; set; }
        [DataMember]
        public string PlanAmount { get; set; }
        [DataMember]
        public string TermAmount { get; set; }
        [DataMember]
        public string NoofTerms { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string PostalCode { get; set; }
        [DataMember]
        public string CountryCode { get; set; }
        [DataMember]
        public string PlanType { get; set; }
        [DataMember]
        public string OutOfPocketMaxFamily { get; set; }
        [DataMember]
        public string OutOfPocketMaxIndividual { get; set; }
        [DataMember]
        public string CoInsurance { get; set; }
        [DataMember]
        public string CoPay { get; set; }
        [DataMember]
        public string Deductable { get; set; }
        [DataMember]
        public string Premium { get; set; }
        [DataMember]
        public string Maternity { get; set; }
        [DataMember]
        public string Gynecology { get; set; }
        [DataMember]
        public string XRay { get; set; }
        [DataMember]
        public string Medication { get; set; }
        [DataMember]
        public string DoctorVisit { get; set; }
        [DataMember]
        public string LabProcedure { get; set; }
        [DataMember]
        public string Mammogram { get; set; }
        [DataMember]
        public string PrescriptionInsulin { get; set; }

        [DataMember]
        public string BenfitPeriod { get; set; }
        [DataMember]
        public string PrimaryCare { get; set; }
        [DataMember]
        public string SpecialistCare { get; set; }
        [DataMember]
        public string DurableMedicalQuipment { get; set; }
        [DataMember]
        public string Diagnostic { get; set; }
        [DataMember]
        public string TheraphyServcies { get; set; }
        [DataMember]
        public string MentalAndSubatanceAbuse { get; set; }
        [DataMember]
        public string DeductableFamily { get; set; }
    }

    [DataContract]
    public class BenefitPlanRequestDto
    {
        [DataMember]
        public string PlanID { get; set; }
        [DataMember]
        public string PlanName { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public int FromAge { get; set; }
        [DataMember]
        public int ToAge { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Country { get; set; }
        [DataMember]
        public int NumberofDependants { get; set; }
        [DataMember]
        public string PremiumAmount { get; set; }
        [DataMember]
        public string PlanAmount { get; set; }
        [DataMember]
        public string Term { get; set; }
        [DataMember]
        public string DeductableAmount { get; set; }
        [DataMember]
        public string PlanType { get; set; }
        [DataMember]
        public string OutOfPocketMax { get; set; }
        [DataMember]
        public string CoInsurance { get; set; }
        [DataMember]
        public string CoPay { get; set; }
        [DataMember]
        public string Deductable { get; set; }

        [DataMember]
        public string Premium { get; set; }
        [DataMember]
        public string Maternity { get; set; }
        [DataMember]
        public string Gynecology { get; set; }
        [DataMember]
        public string XRay { get; set; }
        [DataMember]
        public string Medication { get; set; }
        [DataMember]
        public string DoctorVisit { get; set; }
        [DataMember]
        public string LabProcedure { get; set; }
        [DataMember]
        public string Mammogram { get; set; }
        [DataMember]
        public string PrescriptionInsulin { get; set; }

    }

    [DataContract]
    public class BenefitPlan : DataHelper
    {
        public List<BenefitPlanDto> GetBenfitPlans(BenefitPlanRequestDto request)
        {
            List<BenefitPlanDto> lstBenfitPlans = new List<BenefitPlanDto>();
            DataSet ds = GetDataSet("usp_GetBenfitPlans", new SqlParameter("@Gender",request.Gender==null?"":request.Gender),
                                                          new SqlParameter("@PlanName", request.PlanName==null?"":request.PlanName),
                                                          new SqlParameter("@PlanID", request.PlanID == null ? "" : request.PlanID),
                                                          new SqlParameter("@FromAge", request.FromAge),
                                                          new SqlParameter("@ToAge", request.ToAge),
                                                          new SqlParameter("@City",request.City==null?"":request.City),
                                                          new SqlParameter("@State", request.State==null ? "" : request.State),
                                                          new SqlParameter("@Country",request.Country==null?"":request.Country),
                                                          new SqlParameter("@NumofDependants",request.NumberofDependants),
                                                          new SqlParameter("@DeductableAmount",request.DeductableAmount==null?"":request.DeductableAmount),
                                                          new SqlParameter("@premiumAmount",request.PremiumAmount==null?"":request.PremiumAmount),
                                                          new SqlParameter("@PlanAmount",request.PlanAmount==null?"":request.PlanName),
                                                          new SqlParameter("@Term",request.Term==null?"":request.Term),
                                                          new SqlParameter("@PlanType",request.PlanType==null?"":request.PlanType),
                                                          new SqlParameter("@OOPM",request.OutOfPocketMax==null?"":request.OutOfPocketMax),
                                                          new SqlParameter("@Coinsurance",request.CoInsurance==null?"":request.CoInsurance),                                                          
                                                          new SqlParameter("@Deductable",request.Deductable==null?"":request.Deductable),
                                                          new SqlParameter("@Premium", request.Premium == null ? "" : request.Premium),
                                                          new SqlParameter("@Maternity", request.Maternity == null ? "" : request.Maternity),
                                                          new SqlParameter("@Gynecology", request.Gynecology == null ? "" : request.Gynecology),
                                                          new SqlParameter("@XRay", request.XRay == null ? "" : request.XRay),
                                                          new SqlParameter("@Medication", request.Medication == null ? "" : request.Medication),
                                                          new SqlParameter("@DoctorVisit", request.DoctorVisit == null ? "" : request.DoctorVisit),
                                                          new SqlParameter("@LabProcedure", request.LabProcedure == null ? "" : request.LabProcedure),
                                                          new SqlParameter("@Mammogram", request.Mammogram == null ? "" : request.Mammogram),
                                                          new SqlParameter("@PrescriptionInsulin", request.PrescriptionInsulin == null ? "" : request.PrescriptionInsulin));

            DataTable dt = ds.Tables[0];
            lstBenfitPlans = dt.ToCollection<BenefitPlanDto>();
           return lstBenfitPlans;
        }

		public List<BenefitPlanDto> GetAllBenefitPlans()
		{
			List<BenefitPlanDto> lstBenfitPlans = new List<BenefitPlanDto>();
			DataSet ds = GetAllDataSet("select * from [Watson].[dbo].[BenfitPlans]");
			DataTable dt = ds.Tables[0];
			lstBenfitPlans = dt.ToCollection<BenefitPlanDto>();
			return lstBenfitPlans;

		}

	}

    [DataContract]
    public class SearchFilter
    {
        bool boolValue = true;
        string stringValue = "Hello ";
        [DataMember]
        public object RequestObject
        {   get;
            set;
        }
        [DataMember]
        public object ResponseObject
        {
            get;
            set;
        }
    }
}