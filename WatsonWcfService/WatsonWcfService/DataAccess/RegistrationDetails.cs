﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace WatsonWcfService.DataAccess
{
    [DataContract]
    public class UserProfileDTO
    {
        [DataMember]
        public string Name { get; set; }

		[DataMember]
		public string Username { get; set; }

		[DataMember]
        public string EmailID { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public int UserID { get; set; }
               
        [DataMember]
        public string Premium { get; set; }
        [DataMember]
        public string Deductible { get; set; }
        [DataMember]
        public string IsSmoking { get; set; }
        [DataMember]
        public string IsMajorMedicalEvent { get; set; }
        [DataMember]
        public string IsCancerTreatmentPlanned { get; set; }
        [DataMember]
        public string IsExpectingMother { get; set; }
		[DataMember]
		public string BloodPressure { get; set; }
		[DataMember]
		public string CardiacArrest { get; set; }
		[DataMember]
        public string AgeRange { get; set; }

        [DataMember]
        public string Gender { get; set; }
        
        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string City { get; set; }



    }
    public class RegistrationDetails : DataHelper
    {
        public int RegisterUser(string UserName,string Email,string pwd)
        {
            string sql = string.Format("Insert into Userprofile(Username, EmailID, Password) values ('{0}','{1}','{2}');SELECT CAST(scope_identity() AS int)", UserName, Email, pwd);
            int inserted = InsertRecord(sql);
            return inserted;
        }

        public int CheckUserLogin(string username, string password)
        {
            string sql = string.Format("select UserID from Userprofile where Username='{0}' and Password='{1}'", username, password);
            DataSet ds = GetAllDataSet(sql);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count!=0)
                return Convert.ToInt32(dt.Rows[0]["UserID"]);
            else
                return 0;
        }

        public List<UserProfileDTO> GetUserProfile(string username)
        {
            List<UserProfileDTO> lstRegistrationDetails = new List<UserProfileDTO>();
            DataSet ds = GetAllDataSet(string.Format("select * from Userprofile where Username='{0}'", username));
          
            DataTable dt = ds.Tables[0];
            lstRegistrationDetails = dt.ToCollection<UserProfileDTO>();
         
            foreach (var userprofile in lstRegistrationDetails)
            {
                if(userprofile.AgeRange==null || userprofile.AgeRange==string.Empty)
                {
                    userprofile.AgeRange = "NA";
                }
                if (userprofile.Gender == null || userprofile.Gender == string.Empty)
                {
                    userprofile.Gender = "NA";
                }
                if (userprofile.Premium == null || userprofile.Premium == string.Empty)
                {
                    userprofile.Premium = "0";
                }
           

                if (userprofile.Deductible == null|| userprofile.Deductible == string.Empty)
                {
                    userprofile.Deductible = "0";
                }
              

                if (userprofile.IsSmoking == "no" || userprofile.IsSmoking == null || userprofile.IsSmoking == "false" || userprofile.IsSmoking == "0" || userprofile.IsSmoking == string.Empty)
                {
                    userprofile.IsSmoking = "No";
                }
                else
                {
                    userprofile.IsSmoking = "Yes";
                }
                if (userprofile.IsMajorMedicalEvent == "no" || userprofile.IsMajorMedicalEvent == null || userprofile.IsMajorMedicalEvent == "false" || userprofile.IsMajorMedicalEvent == "0" || userprofile.IsMajorMedicalEvent==string.Empty)
                {
                    userprofile.IsMajorMedicalEvent = "No";
                }
                else
                {
                    userprofile.IsMajorMedicalEvent = "Yes";
                }
                if (userprofile.IsCancerTreatmentPlanned == "no" || userprofile.IsCancerTreatmentPlanned == null || userprofile.IsCancerTreatmentPlanned == "false" || userprofile.IsCancerTreatmentPlanned == "0" || userprofile.IsCancerTreatmentPlanned == string.Empty)
                {
                    userprofile.IsCancerTreatmentPlanned = "No";
                }
                else
                {
                    userprofile.IsCancerTreatmentPlanned = "Yes";
                }

                if (userprofile.IsExpectingMother == "no" || userprofile.IsExpectingMother == null || userprofile.IsExpectingMother == "false" || userprofile.IsExpectingMother=="0" || userprofile.IsExpectingMother==string.Empty)
                {
                    userprofile.IsExpectingMother = "No";
                }
                else
                {
                    userprofile.IsExpectingMother = "Yes";
                }

				if (userprofile.BloodPressure == "no" || userprofile.BloodPressure == null || userprofile.BloodPressure == "false" || userprofile.BloodPressure == "0" || userprofile.BloodPressure == string.Empty)
				{
					userprofile.BloodPressure = "No";
				}
				else
				{
					userprofile.BloodPressure = "Yes";
				}

				if (userprofile.CardiacArrest == "no" || userprofile.CardiacArrest == null || userprofile.CardiacArrest == "false" || userprofile.CardiacArrest == "0" || userprofile.CardiacArrest == string.Empty)
				{
					userprofile.CardiacArrest = "No";
				}
				else
				{
					userprofile.CardiacArrest = "Yes";
				}

                if (userprofile.Country == null || userprofile.Country == string.Empty)
                {
                    userprofile.Country = "NA";
                }
                if (userprofile.State == null || userprofile.State == string.Empty)
                {
                    userprofile.State = "NA";
                }
                if (userprofile.City == null || userprofile.City == string.Empty)
                {
                    userprofile.City = "NA";
                }
            }
            return lstRegistrationDetails;
        }

		public int UpdateProfile(string username, string premium, string deductible, string medicalEvents, string tobacco, string bloodPressure,
			string cardiacArrest, string pregnancy, string breastCancer)
		{
			List<UserProfileDTO> lstRegistrationDetails = new List<UserProfileDTO>();
			int updated = UpdateRecord("usp_UpdateProfile_2",
				new SqlParameter("@Username", username == null ? "" : username),
				new SqlParameter("@Premium", premium == null ? "" : premium),
				new SqlParameter("@Deductible", deductible == null ? "" : deductible),
				new SqlParameter("@IsSmoking", tobacco == null ? "" : tobacco),
				new SqlParameter("@IsMajorMedicalEvent", medicalEvents == null ? "" : medicalEvents),
				new SqlParameter("@IsCancerTreatmentPlanned", breastCancer == null ? "" : breastCancer),
				new SqlParameter("@IsExpectingMother", pregnancy == null ? "" : pregnancy),
				new SqlParameter("@CardiacArrest", cardiacArrest == null ? "" : cardiacArrest),
				new SqlParameter("@BloodPressure", bloodPressure == null ? "" : bloodPressure)
				);
			return updated;
		}

		public int UpdateAgeGender(string username, string ageRange, string gender)
        {
            List<UserProfileDTO> lstRegistrationDetails = new List<UserProfileDTO>();
            int updated = UpdateRecord("[usp_UpdateAgeGender]", new SqlParameter("@Username", username),
                new SqlParameter("@AgeRange", ageRange== null ? "" : ageRange),
                new SqlParameter("@Gender", gender == null ? "" : gender)
                );
            return updated;
        }

        public int UpdateLocInfo(string username, string country, string state, string city)
        {
            List<UserProfileDTO> lstRegistrationDetails = new List<UserProfileDTO>();
            int updated = UpdateRecord("usp_UpdateLocInfo", new SqlParameter("@Username", username),
                new SqlParameter("@Country", country == null ? "" : country),
                new SqlParameter("@State", state == null ? "" : state),
                new SqlParameter("@City", city == null ? "" : city)
                );
            return updated;
        }
    }
}




